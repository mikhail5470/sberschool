package org.example.homework13;

import java.util.Random;

public class RandomStringProducer {

    private int current = 0;

    private final ArrayBlockingStack<String> storage;

    public void run() {
        while (true) {
            try {
                storage.put(produce());
            } catch (InterruptedException interruptedException) {
                return;
            }
        }
    }

    public RandomStringProducer(ArrayBlockingStack<String> storage) {
        this.storage = storage;
    }

    private String produce() {
        try {
            Thread.sleep(2000 + new Random().nextInt(500));
        } catch (InterruptedException ignore) {
        }

        current++;
        String item = String.format("item #%d (by %s)", current, this);
        System.out.println(String.format("[produced] %s", item));

        return item;
    }

}
