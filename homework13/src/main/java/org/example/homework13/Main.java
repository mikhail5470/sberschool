package org.example.homework13;

public class Main {

    public static void main(String[] args) {

        ArrayBlockingStack<String> storage = new ArrayBlockingStack<>(3);
        RandomStringProducer producer1 = new RandomStringProducer(storage);
        RandomStringProducer producer2 = new RandomStringProducer(storage);
        RandomStringProducer producer3 = new RandomStringProducer(storage);
        RandomStringConsumer consumer = new RandomStringConsumer(storage);

        new Thread(producer1::run).start();
        new Thread(producer2::run).start();
        new Thread(producer3::run).start();
        new Thread(consumer::run).start();

    }
}
