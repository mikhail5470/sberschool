package org.example.homework13;

import java.util.Random;

public class RandomStringConsumer {

    private final ArrayBlockingStack<String> storage;

    public RandomStringConsumer(ArrayBlockingStack<String> storage) {
        this.storage = storage;
    }

    public void run() {
        while (true) {
            try {
                consume(storage.take());
            } catch (InterruptedException interruptedException) {
                return;
            }
        }
    }

    private void consume(String item) {
        try {
            Thread.sleep(2000 + new Random().nextInt(500));
        } catch (InterruptedException ignore) {
        }
        System.out.println("\t[consumed] " + item);
    }
}
