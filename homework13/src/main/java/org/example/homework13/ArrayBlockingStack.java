package org.example.homework13;

import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ArrayBlockingStack<T> {

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock writeLock = readWriteLock.writeLock();
    private final Lock readLock = readWriteLock.readLock();

    private final Semaphore takeSemaphore;
    private final Semaphore putSemaphore;

    private final T[] items;
    private int count;

    public ArrayBlockingStack(int capacity) {
        if (capacity < 1) throw new IllegalArgumentException("capacity must be positive");

        this.takeSemaphore = new Semaphore(0);
        this.putSemaphore = new Semaphore(capacity);

        this.count = 0;
        this.items = (T[]) new Object[capacity];
    }

    public ArrayBlockingStack() {
        this(Integer.MAX_VALUE);
    }

    public T take() throws InterruptedException {
        takeSemaphore.acquire();
        writeLock.lock();
        try {
            T item = items[count - 1];
            count--;
            putSemaphore.release();
            System.out.println("[TAKE] size: " + count);
            return item;
        } finally {
            writeLock.unlock();
        }
    }

    public void put(T item) throws InterruptedException {
        putSemaphore.acquire();
        writeLock.lock();
        try {
            items[count] = item;
            count++;
            System.out.println("[PUT] size: " + count);
            takeSemaphore.release();
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public String toString() {
        readLock.lock();
        try {
            return Arrays.toString(items);
        } finally {
            readLock.unlock();
        }
    }

}
