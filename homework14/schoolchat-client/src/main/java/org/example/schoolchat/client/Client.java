package org.example.schoolchat.client;

import org.example.schoolchat.client.connector.SocketConnector;
import org.example.schoolchat.client.connector.SocketConnectorListener;
import org.example.schoolchat.util.command.Command;
import org.example.schoolchat.util.view.View;
import org.example.schoolchat.util.view.ViewListener;
import org.example.schoolchat.util.view.impl.ConsoleView;

import java.net.InetSocketAddress;
import java.util.Arrays;

public class Client implements ViewListener, SocketConnectorListener {

    private static final String HOST = "localhost";
    private static final int PORT = 1337;
    private final View view;
    private final SocketConnector socketConnector;

    public Client() {
        this.view = new ConsoleView();
        this.view.addListener(this);
        this.socketConnector = new SocketConnector(new InetSocketAddress(HOST, PORT));
        this.socketConnector.addListener(this);
    }

    public static void main(String[] args) {
        new Client();
    }

    public View getView() {
        return view;
    }

    public SocketConnector getSocketConnector() {
        return socketConnector;
    }

    @Override
    public void onInput(String message) {
        message = message.trim();
        if (message.equals("")) return;

        if (message.charAt(0) == '/') {
            message = message.substring(1);
            String[] tokens = message.split(" ");
            socketConnector.say(new Command(tokens[0], Arrays.copyOfRange(tokens, 1, tokens.length)));
        } else {

            String[] tokens = message.split(" ");
            socketConnector.say(new Command("say", new String[]{message}));
        }
    }

    @Override
    public void onConnect() {
        view.output("connected");
    }

    @Override
    public void onSay(Command command) {
        if (command.getName().equals("say")) {
            view.output(command.getArgs()[0]);
        }
    }

    @Override
    public void onDisconnect() {
        view.output("disconnected");
    }
}
