package org.example.schoolchat.client.connector;

import org.example.schoolchat.util.command.Command;
import org.example.schoolchat.util.listener.Listenable;
import org.example.schoolchat.util.socket.CommandPacket;
import org.example.schoolchat.util.socket.HeartBeat;
import org.example.schoolchat.util.socket.SocketPacket;

import java.io.*;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashSet;
import java.util.Set;

public class SocketConnector implements Closeable, Listenable<SocketConnectorListener> {

    private final SocketAddress address;
    private final Set<SocketConnectorListener> listeners;
    private boolean closed;
    private boolean connected;
    private Socket socket;

    public SocketConnector(SocketAddress address) {
        this.closed = false;
        this.connected = false;
        this.listeners = new HashSet<>();
        this.address = address;
        new Thread(new Connector()).start();
        new Thread(new Listener()).start();
    }

    private void onConnected() {
        for (SocketConnectorListener listener : listeners) {
            listener.onConnect();
        }
    }

    private void onSay(Command command) {
        for (SocketConnectorListener listener : listeners) {
            listener.onSay(command);
        }
    }

    private void onDisconnect() {
        for (SocketConnectorListener listener : listeners) {
            listener.onDisconnect();
        }
    }

    private void send(SocketPacket packet) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(packet);
        oos.close();
        socket.getOutputStream().write(baos.toByteArray());
    }

    public void say(Command command) {
        if (closed) throw new IllegalStateException("closed");

        try {
            send(new CommandPacket(command));
        } catch (IOException ignore) {
        }
    }

    @Override
    public void close() throws IOException {
        if (closed) throw new IllegalStateException("already closed");

        closed = true;
        socket.close();
    }

    @Override
    public void addListener(SocketConnectorListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(SocketConnectorListener listener) {
        listeners.remove(listener);
    }

    private class Connector implements Runnable {
        private final int HEART_BEAT_TIMEOUT = 1000;

        @Override
        public void run() {
            while (!closed) {
                while (!connected) {
                    try {
                        socket = new Socket();
                        socket.connect(address);
                        connected = true;
                        onConnected();
                    } catch (IOException e) {
                        try {
                            socket.close();
                        } catch (IOException ignore) {
                        } finally {
                            connected = false;
                        }
                    }
                }

                try {
                    send(new HeartBeat());
                    try {
                        Thread.sleep(HEART_BEAT_TIMEOUT);
                    } catch (InterruptedException ignore) {
                    }
                } catch (IOException e) {
                    connected = false;
                    onDisconnect();
                }
            }
        }
    }

    private class Listener implements Runnable {
        @Override
        public void run() {
            while (!closed) {
                while (connected) {
                    try {
                        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

                        try {
                            SocketPacket packet = (SocketPacket) ois.readObject();
                            if (packet instanceof CommandPacket) {
                                CommandPacket commandPacket = (CommandPacket) packet;
                                onSay(commandPacket.getCommand());
                            }
                        } catch (ClassNotFoundException | ClassCastException ignore) {
                        }
                    } catch (IOException ignore) {
                    }
                }
            }
        }
    }
}
