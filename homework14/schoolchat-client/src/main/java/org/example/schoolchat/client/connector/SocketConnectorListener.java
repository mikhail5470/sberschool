package org.example.schoolchat.client.connector;

import org.example.schoolchat.util.command.Command;
import org.example.schoolchat.util.listener.Listener;

public interface SocketConnectorListener extends Listener {

    void onConnect();

    void onSay(Command command);

    void onDisconnect();

}
