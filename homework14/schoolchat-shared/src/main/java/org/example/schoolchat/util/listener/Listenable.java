package org.example.schoolchat.util.listener;

public interface Listenable<T extends Listener> {
    void addListener(T listener);

    void removeListener(T listener);
}
