package org.example.schoolchat.util.view;

import org.example.schoolchat.util.listener.Listenable;

import java.io.Closeable;

public interface View extends Closeable, Listenable<ViewListener> {

    void output(String message);

}
