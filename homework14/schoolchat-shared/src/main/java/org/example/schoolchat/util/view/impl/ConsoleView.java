package org.example.schoolchat.util.view.impl;

import org.example.schoolchat.util.view.View;
import org.example.schoolchat.util.view.ViewListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class ConsoleView implements View {

    private final BufferedReader reader;
    private final Set<ViewListener> listeners;
    private boolean closed;

    public ConsoleView() {
        this.closed = false;
        this.listeners = new HashSet<>();
        this.reader = new BufferedReader(new InputStreamReader(System.in));
        new Thread(new Reader()).start();
    }

    private void onInput(String message) {
        for (ViewListener listener : listeners) {
            listener.onInput(message);
        }
    }

    @Override
    synchronized public void close() {
        if (closed) throw new IllegalStateException("already closed");

        closed = true;
    }

    @Override
    public void output(String message) {
        System.out.println(message);
    }

    @Override
    public void addListener(ViewListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(ViewListener listener) {
        listeners.remove(listener);
    }

    private class Reader implements Runnable {
        @Override
        public void run() {
            while (!closed) {
                try {
                    onInput(reader.readLine());
                } catch (IOException ignore) {
                }
            }
        }
    }
}
