package org.example.schoolchat.util.command;

public interface CommandHandler {

    void handle(String[] args);

}
