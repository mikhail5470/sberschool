package org.example.schoolchat.util.command;

import javax.naming.OperationNotSupportedException;
import java.util.HashMap;
import java.util.Map;

public class CommandManager {

    private final Map<String, CommandHandler> handlers;

    public CommandManager() {
        this.handlers = new HashMap<>();
    }

    public void addHandler(String command, CommandHandler handler) {
        if (handlers.get(command) != null) throw new IllegalArgumentException("already added");

        handlers.put(command, handler);
    }

    public void execute(Command command) throws OperationNotSupportedException {
        CommandHandler handler = handlers.get(command.getName());
        if (handler == null) throw new OperationNotSupportedException(command.getName() + " handler not found");

        handler.handle(command.getArgs());
    }
}
