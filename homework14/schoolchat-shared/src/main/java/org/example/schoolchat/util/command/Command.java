package org.example.schoolchat.util.command;

import java.io.Serializable;
import java.util.Arrays;

public class Command implements Serializable {

    private final String name;
    private final String[] args;

    public Command(String[] args) {
        if (args.length < 1) throw new IllegalArgumentException("empty array");

        this.name = args[0];
        this.args = Arrays.copyOfRange(args, 1, args.length);
    }

    public Command(String name, String[] args) {
        this.name = name;
        this.args = Arrays.copyOf(args, args.length);
    }

    public String getName() {
        return name;
    }

    public String[] getArgs() {
        return Arrays.copyOf(args, args.length);
    }
}
