package org.example.schoolchat.util.socket;

import org.example.schoolchat.util.command.Command;

public class CommandPacket implements SocketPacket {

    private final Command command;

    public CommandPacket(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}
