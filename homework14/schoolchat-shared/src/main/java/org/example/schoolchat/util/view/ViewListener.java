package org.example.schoolchat.util.view;

import org.example.schoolchat.util.listener.Listener;

public interface ViewListener extends Listener {

    void onInput(String message);

}
