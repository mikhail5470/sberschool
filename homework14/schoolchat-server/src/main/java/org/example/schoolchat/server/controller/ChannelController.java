package org.example.schoolchat.server.controller;

import org.example.schoolchat.util.command.Command;
import org.example.schoolchat.util.listener.Listenable;
import org.example.schoolchat.util.socket.CommandPacket;
import org.example.schoolchat.util.socket.SocketPacket;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ChannelController implements Closeable, Listenable<ChannelControllerListener> {

    private final Selector selector;
    private final Set<SocketChannel> channels;
    private final Object registerLock;
    private final Set<ChannelControllerListener> listeners;
    private boolean closed;

    public ChannelController() throws IOException {
        this.selector = Selector.open();
        this.listeners = new HashSet<>();
        this.channels = Collections.synchronizedSet(new HashSet<>());
        this.closed = false;
        this.registerLock = new Object();
        new Thread(new Listener()).start();
    }

    synchronized public void register(SocketChannel channel) {
        try {
            synchronized (registerLock) {
                selector.wakeup();
                channel.configureBlocking(false);
                channel.register(selector, SelectionKey.OP_READ);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        channels.add(channel);
    }

    private void send(SocketChannel channel, SocketPacket packet) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(packet);
        oos.close();
        ByteBuffer wrap = ByteBuffer.wrap(baos.toByteArray());
        channel.write(wrap);
    }

    private void send(SocketPacket packet, SocketChannel ignored) throws IOException {
        for (SocketChannel channel : channels) {
            if (channel == ignored) continue;
            send(channel, packet);
        }
    }

    public void say(Command command, SocketChannel ignored) {
        if (closed) throw new IllegalStateException("closed");

        try {
            send(new CommandPacket(command), ignored);
        } catch (IOException ignore) {
        }
    }

    public void say(SocketChannel channel, Command command) {
        try {
            send(channel, new CommandPacket(command));
        } catch (IOException ignore) {
        }
    }

    public void say(Command command) {
        say(command, null);
    }

    @Override
    synchronized public void close() throws IOException {
        if (closed) return;

        closed = true;
        selector.close();
        for (SocketChannel channel : channels) {
            channel.close();
        }
    }

    @Override
    public void addListener(ChannelControllerListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(ChannelControllerListener listener) {
        listeners.remove(listener);
    }

    private void onSay(SocketChannel channel, Command command) {
        for (ChannelControllerListener listener : listeners) {
            listener.onSay(channel, command);
        }
    }

    private void onDisconnected(SocketChannel channel, DisconnectReason reason) {
        try {
            channel.close();
        } catch (IOException ignore) {
        }
        channels.remove(channel);
        for (ChannelControllerListener listener : listeners) {
            listener.onDisconnect(channel, reason);
        }
    }

    public enum DisconnectReason {
        CLOSED, TIMED_OUT, UNKNOWN
    }

    private class Listener implements Runnable {
        private static final int SELECTOR_TIMEOUT = 1000;

        @Override
        public void run() {
            while (!closed) {
                synchronized (registerLock) {
                }

                try {
                    if (selector.select(SELECTOR_TIMEOUT) == 0) continue;
                } catch (IOException e) {
                    continue;
                }

                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();

                    SocketChannel channel = (SocketChannel) key.channel();
                    if (key.isReadable()) {
                        try {
                            ByteBuffer bb = ByteBuffer.allocate(1024);
                            channel.read(bb);

                            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bb.array()));
                            try {
                                SocketPacket packet = (SocketPacket) ois.readObject();
                                if (packet instanceof CommandPacket) {
                                    CommandPacket commandPacket = (CommandPacket) packet;
                                    onSay(channel, commandPacket.getCommand());
                                }

                            } catch (ClassNotFoundException | ClassCastException ignore) {
                            }

                        } catch (IOException e) {
                            key.cancel();
                            onDisconnected(channel, DisconnectReason.CLOSED);
                        }
                    }
                }
            }
        }
    }
}
