package org.example.schoolchat.server.client.command;

import org.example.schoolchat.server.client.ChatClient;
import org.example.schoolchat.util.command.Command;

import javax.naming.OperationNotSupportedException;
import java.util.HashMap;
import java.util.Map;

public class ClientCommandManager {

    private final Map<String, ClientCommandHandler> handlers;

    public ClientCommandManager() {
        this.handlers = new HashMap<>();
    }

    public void addHandler(String command, ClientCommandHandler handler) {
        if (handlers.get(command) != null) throw new IllegalArgumentException("already added");

        handlers.put(command, handler);
    }

    public void execute(ChatClient client, Command command) throws OperationNotSupportedException {
        ClientCommandHandler handler = handlers.get(command.getName());
        if (handler == null) throw new OperationNotSupportedException(command.getName() + " handler not found");

        handler.handle(client, command.getArgs());
    }

}
