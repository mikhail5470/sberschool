package org.example.schoolchat.server.command;

import org.example.schoolchat.server.Server;
import org.example.schoolchat.util.command.CommandHandler;

public abstract class AbstractServerCommandHandler implements CommandHandler {

    private final Server server;

    public AbstractServerCommandHandler(Server server) {
        this.server = server;
    }

    public Server getServer() {
        return server;
    }
}
