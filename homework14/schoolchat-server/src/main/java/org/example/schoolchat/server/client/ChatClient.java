package org.example.schoolchat.server.client;

import java.nio.channels.SocketChannel;

public class ChatClient {

    private static final String DEFAULT_NAME = "guest";
    private final SocketChannel channel;
    private State state;
    private String name;

    public ChatClient(SocketChannel channel) {
        this.channel = channel;
        this.state = State.CONNECTED;
        this.name = DEFAULT_NAME;
    }

    public SocketChannel getChannel() {
        return channel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public enum State {
        CONNECTED, AUTHORIZED
    }
}
