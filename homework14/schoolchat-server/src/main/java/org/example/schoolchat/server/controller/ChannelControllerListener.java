package org.example.schoolchat.server.controller;

import org.example.schoolchat.util.command.Command;
import org.example.schoolchat.util.listener.Listener;

import java.nio.channels.SocketChannel;

public interface ChannelControllerListener extends Listener {

    void onSay(SocketChannel channel, Command command);

    void onDisconnect(SocketChannel channel, ChannelController.DisconnectReason reason);

}
