package org.example.schoolchat.server.command.impl;

import org.example.schoolchat.server.Server;
import org.example.schoolchat.server.command.AbstractServerCommandHandler;
import org.example.schoolchat.util.command.Command;

public class BroadcastCommandHandler extends AbstractServerCommandHandler {

    public BroadcastCommandHandler(Server server) {
        super(server);
    }

    @Override
    public void handle(String[] args) {
        getServer().getChannelController().say(new Command("say", args));
    }
}
