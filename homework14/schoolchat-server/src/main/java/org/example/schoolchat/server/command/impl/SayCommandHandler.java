package org.example.schoolchat.server.command.impl;

import org.example.schoolchat.server.Server;
import org.example.schoolchat.server.client.ChatClient;
import org.example.schoolchat.server.command.AbstractServerCommandHandler;
import org.example.schoolchat.util.command.Command;

import java.util.Arrays;

public class SayCommandHandler extends AbstractServerCommandHandler {
    public SayCommandHandler(Server server) {
        super(server);
    }

    @Override
    public void handle(String[] args) {
        if (args.length < 2) throw new IllegalArgumentException();

        ChatClient target = getServer().getClient(args[0]);
        if (target == null) throw new IllegalArgumentException();

        String message = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        getServer().getChannelController().say(target.getChannel(), new Command("say", new String[]{message}));
    }
}
