package org.example.schoolchat.server;

import org.example.schoolchat.server.acceptor.ChannelAcceptor;
import org.example.schoolchat.server.acceptor.ChannelAcceptorListener;
import org.example.schoolchat.server.client.ChatClient;
import org.example.schoolchat.server.client.command.ClientCommandManager;
import org.example.schoolchat.server.client.command.impl.PmClientCommandHandler;
import org.example.schoolchat.server.client.command.impl.SayClientCommandHandler;
import org.example.schoolchat.server.command.impl.BroadcastCommandHandler;
import org.example.schoolchat.server.command.impl.SayCommandHandler;
import org.example.schoolchat.server.controller.ChannelController;
import org.example.schoolchat.server.controller.ChannelControllerListener;
import org.example.schoolchat.util.command.Command;
import org.example.schoolchat.util.command.CommandManager;
import org.example.schoolchat.util.view.View;
import org.example.schoolchat.util.view.ViewListener;
import org.example.schoolchat.util.view.impl.ConsoleView;

import javax.naming.OperationNotSupportedException;
import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Server implements ViewListener, ChannelAcceptorListener, ChannelControllerListener {

    private static final int PORT = 1337;

    private final View view;
    private final CommandManager commandManager;
    private final ClientCommandManager clientCommandManager;
    private final ChannelAcceptor channelAcceptor;
    private final ChannelController channelController;
    private final Set<ChatClient> clients;

    public Server() throws IOException {
        this.clients = new HashSet<>();
        this.view = new ConsoleView();
        this.view.addListener(this);
        this.commandManager = new CommandManager();
        this.commandManager.addHandler("broadcast", new BroadcastCommandHandler(this));
        this.commandManager.addHandler("say", new SayCommandHandler(this));
        this.clientCommandManager = new ClientCommandManager();
        this.clientCommandManager.addHandler("say", new SayClientCommandHandler(this));
        this.clientCommandManager.addHandler("pm", new PmClientCommandHandler(this));
        this.channelController = new ChannelController();
        this.channelController.addListener(this);
        this.channelAcceptor = new ChannelAcceptor(PORT);
        this.channelAcceptor.addListener(this);
    }

    public static void main(String[] args) throws IOException {
        new Server();
    }

    public View getView() {
        return view;
    }

    public ChannelController getChannelController() {
        return channelController;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    private ChatClient getClient(SocketChannel channel) {
        for (ChatClient client : clients) {
            if (client.getChannel().equals(channel)) return client;
        }
        return null;
    }

    public ChatClient getClient(String name) {
        for (ChatClient client : clients) {
            if (client.getName().equals(name)) return client;
        }
        return null;
    }

    @Override
    public void onInput(String message) {
        String[] tokens = message.split(" ");
        try {
            commandManager.execute(new Command(tokens[0], Arrays.copyOfRange(tokens, 1, tokens.length)));
        } catch (OperationNotSupportedException ignore) {
            view.output("invalid command");
        }
    }

    @Override
    public void onAccepted(SocketChannel channel) {
        channelController.register(channel);
        clients.add(new ChatClient(channel));
        channelController.say(channel, new Command("say", new String[]{"valid commands: say, pm. Example: /pm test1 hi"}));
        channelController.say(channel, new Command("say", new String[]{"server: say your name"}));
        view.output("client connected");
    }

    @Override
    public void onSay(SocketChannel channel, Command command) {
        ChatClient client = getClient(channel);

        try {
            clientCommandManager.execute(client, command);
        } catch (OperationNotSupportedException e) {
            channelController.say(channel, new Command("say", new String[]{"server: command not found"}));
        } catch (IllegalArgumentException e) {
            channelController.say(channel, new Command("say", new String[]{"server: invalid args"}));
        }
    }

    @Override
    public void onDisconnect(SocketChannel channel, ChannelController.DisconnectReason reason) {
        view.output("client disconnected: " + reason);
        ChatClient client = getClient(channel);
        clients.remove(client);
        String message = String.format("server: %s is out", client.getName());
        try {
            commandManager.execute(new Command("broadcast", new String[]{message}));
        } catch (OperationNotSupportedException ignore) {
        }
    }
}
