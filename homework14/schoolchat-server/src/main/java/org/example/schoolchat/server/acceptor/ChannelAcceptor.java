package org.example.schoolchat.server.acceptor;

import org.example.schoolchat.util.listener.Listenable;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashSet;
import java.util.Set;

public class ChannelAcceptor implements Closeable, Listenable<ChannelAcceptorListener> {

    private final ServerSocketChannel serverChannel;
    private final Set<ChannelAcceptorListener> listeners;

    public ChannelAcceptor(int port) throws IOException {
        this.listeners = new HashSet<>();
        this.serverChannel = ServerSocketChannel.open();
        this.serverChannel.bind(new InetSocketAddress(port));
        new Thread(new Waiter()).start();
    }

    public ServerSocketChannel getChannel() {
        return serverChannel;
    }

    @Override
    public void close() throws IOException {
        serverChannel.close();
    }

    public void addListener(ChannelAcceptorListener listener) {
        listeners.add(listener);
    }

    public void removeListener(ChannelAcceptorListener listener) {
        listeners.remove(listener);
    }

    private void register(SocketChannel channel) {
        for (ChannelAcceptorListener listener : listeners) {
            listener.onAccepted(channel);
        }
        System.out.println("accepted");
    }

    private class Waiter implements Runnable {
        @Override
        public void run() {
            while (serverChannel.isOpen()) {
                try {
                    register(serverChannel.accept());
                } catch (IOException ignore) {
                }
            }
        }
    }

}
