package org.example.schoolchat.server.acceptor;

import org.example.schoolchat.util.listener.Listener;

import java.nio.channels.SocketChannel;

public interface ChannelAcceptorListener extends Listener {

    void onAccepted(SocketChannel socket);

}
