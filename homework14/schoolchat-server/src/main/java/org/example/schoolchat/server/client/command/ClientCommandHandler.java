package org.example.schoolchat.server.client.command;

import org.example.schoolchat.server.client.ChatClient;

public interface ClientCommandHandler {

    void handle(ChatClient client, String[] args);

}
