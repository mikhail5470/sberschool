package org.example.schoolchat.server.client.command.impl;

import org.example.schoolchat.server.Server;
import org.example.schoolchat.server.client.ChatClient;
import org.example.schoolchat.server.client.command.AbstractClientCommandHandler;
import org.example.schoolchat.util.command.Command;

import javax.naming.OperationNotSupportedException;

public class SayClientCommandHandler extends AbstractClientCommandHandler {

    public SayClientCommandHandler(Server server) {
        super(server);
    }

    @Override
    public void handle(ChatClient client, String[] args) {
        String message = args[0];

        if (client.getState() == ChatClient.State.CONNECTED) {
            if (!message.trim().equals("")) {
                client.setName(message);
            }
            client.setState(ChatClient.State.AUTHORIZED);
            try {
                getServer().getCommandManager().execute(new Command("broadcast", new String[]{String.format("server: %s is here. Welcome!", client.getName())}));
            } catch (OperationNotSupportedException ignore) {
            }

        } else {
            getServer().getView().output(String.format("client: %s: %s", client.getName(), message));
            getServer().getChannelController().say(new Command("say", new String[]{String.format("%s: %s", client.getName(), message)}));

        }
    }
}
