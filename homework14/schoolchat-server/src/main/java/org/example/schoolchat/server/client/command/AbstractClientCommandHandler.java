package org.example.schoolchat.server.client.command;

import org.example.schoolchat.server.Server;

public abstract class AbstractClientCommandHandler implements ClientCommandHandler {

    private final Server server;

    public AbstractClientCommandHandler(Server server) {
        this.server = server;
    }

    public Server getServer() {
        return server;
    }
}
