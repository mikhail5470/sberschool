package org.example.schoolchat.server.client.command.impl;

import org.example.schoolchat.server.Server;
import org.example.schoolchat.server.client.ChatClient;
import org.example.schoolchat.server.client.command.AbstractClientCommandHandler;
import org.example.schoolchat.util.command.Command;

import java.util.Arrays;

public class PmClientCommandHandler extends AbstractClientCommandHandler {

    public PmClientCommandHandler(Server server) {
        super(server);
    }

    @Override
    public void handle(ChatClient client, String[] args) {
        if (args.length < 2) return;

        String name = args[0];
        ChatClient target = getServer().getClient(name);
        if (target == null) {
            getServer().getChannelController().say(client.getChannel(), new Command("say", new String[]{"server: sorry, client not found"}));
            return;
        }

        String message = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        getServer().getChannelController().say(target.getChannel(), new Command("say", new String[]{String.format("PM from %s: %s", client.getName(), message)}));
        getServer().getChannelController().say(client.getChannel(), new Command("say", new String[]{String.format("PM to %s: %s", name, message)}));
    }
}
