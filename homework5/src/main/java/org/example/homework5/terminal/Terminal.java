package org.example.homework5.terminal;

import org.example.homework5.main.exception.CardIsLockedException;
import org.example.homework5.main.exception.InvalidCardException;
import org.example.homework5.main.exception.InvalidPinException;
import org.example.homework5.main.exception.NotEnoughMoneyException;
import org.example.homework5.terminal.exception.AlreadyAuthorizedException;
import org.example.homework5.terminal.exception.InvalidCashException;
import org.example.homework5.terminal.exception.NotAuthorizedException;

public interface Terminal {

    void authorize(String cardID, String pin) throws CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException;
    Double getBalance() throws NotAuthorizedException;
    void putCash(Double amount) throws NotAuthorizedException, InvalidCashException;
    void takeCash(Double amount) throws NotAuthorizedException, InvalidCashException, NotEnoughMoneyException;
    void exit() throws NotAuthorizedException;

}
