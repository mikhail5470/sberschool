package org.example.homework5.terminal.exception;

public class NotAuthorizedException extends Exception {

    public NotAuthorizedException() {
        super("authorize first");
    }

    public NotAuthorizedException(String message) {
        super(message);
    }

    public NotAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAuthorizedException(Throwable cause) {
        super(cause);
    }
}
