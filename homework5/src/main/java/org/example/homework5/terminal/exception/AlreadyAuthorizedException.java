package org.example.homework5.terminal.exception;

public class AlreadyAuthorizedException extends Exception {

    public AlreadyAuthorizedException() {
        super("don't try authorize more");
    }

    public AlreadyAuthorizedException(String message) {
        super(message);
    }

    public AlreadyAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlreadyAuthorizedException(Throwable cause) {
        super(cause);
    }
}
