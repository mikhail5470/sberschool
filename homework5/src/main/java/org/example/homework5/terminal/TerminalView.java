package org.example.homework5.terminal;

public interface TerminalView {

    void outputMessage(String message);

}
