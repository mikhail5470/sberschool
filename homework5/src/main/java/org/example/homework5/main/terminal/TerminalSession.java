package org.example.homework5.main.terminal;

import org.example.homework5.main.model.Card;

public class TerminalSession {

    private final Card card;

    public TerminalSession(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }
}
