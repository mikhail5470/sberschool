package org.example.homework5.main;

import org.example.homework5.main.terminal.TerminalServer;

public class Bank {

    private final ClientManager clientManager;
    private final AccountManager accountManager;
    private final CardManager cardManager;
    private final TerminalServer terminalServer;

    public Bank(ClientManager clientManager, AccountManager accountManager, CardManager cardManager) {
        this.clientManager = clientManager;
        this.accountManager = accountManager;
        this.cardManager = cardManager;
        this.terminalServer = new TerminalServer(this);
    }

    public ClientManager getClientManager() {
        return clientManager;
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }

    public CardManager getCardManager() {
        return cardManager;
    }

    public TerminalServer getTerminalServer() {
        return terminalServer;
    }
}
