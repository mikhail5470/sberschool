package org.example.homework5.main;

import org.example.homework5.main.model.Account;
import org.example.homework5.main.model.Card;

import java.util.ArrayList;
import java.util.Timer;

public class CardManager {

    private static final int LOCK_CARD_INTERVAL = 5000;

    private final ArrayList<Card> cards;
    private final Timer lockTimer;

    public CardManager(ArrayList<Card> cards) {
        this.cards = cards;
        this.lockTimer = new Timer();
    }

    public ArrayList<Card> getCards() {
        return new ArrayList<>(cards);
    }

    public Card getCard(String id) {
        for (Card card : cards) {
            if (card.getId().equals(id)) return card;
        }
        return null;
    }

    public ArrayList<Card> getCards(Account account) {
        ArrayList<Card> accountCards = new ArrayList<>();
        for (Card card : cards) {
            if (card.getAccount() == account) accountCards.add(card);
        }
        return accountCards;
    }

}
