package org.example.homework5.main.terminal;

import org.example.homework5.main.*;
import org.example.homework5.main.exception.CardIsLockedException;
import org.example.homework5.main.exception.InvalidCardException;
import org.example.homework5.main.exception.InvalidPinException;
import org.example.homework5.main.exception.NotEnoughMoneyException;
import org.example.homework5.main.model.Account;
import org.example.homework5.main.model.Card;
import org.example.homework5.terminal.Terminal;
import org.example.homework5.terminal.TerminalImpl;

import java.time.Duration;
import java.util.Date;

public class TerminalServer {

    private static final int LOCK_CARD_ATTEMPTS = 3;
    private static final Duration LOCK_CARD_DURATION = Duration.ofSeconds(5L);

    private final Bank bank;
    private final Terminal terminal;

    public TerminalServer(Bank bank) {
        this.bank = bank;
        this.terminal = new TerminalImpl(this);
    }

    public TerminalSession authorize(String cardID, String pin) throws InvalidCardException, InvalidPinException, CardIsLockedException {
        Card card = bank.getCardManager().getCard(cardID);
        if (card == null) throw new InvalidCardException();
        if (card.isLocked()) throw new CardIsLockedException(card.getUnlockDate());
        if (!card.getPin().equals(pin)) {
            int attempts = card.getAttempts() + 1;
            card.setAttempts(attempts);
            if (attempts >= LOCK_CARD_ATTEMPTS) {
                card.setUnlockDate(new Date(System.currentTimeMillis() + LOCK_CARD_DURATION.toMillis()));
            }
            throw new InvalidPinException();
        }

        card.setAttempts(0);

        return new TerminalSession(card);
    }

    public Double getBalance(TerminalSession session) {
        return session.getCard().getAccount().getBalance();
    }

    public void addCash(TerminalSession session, Double amount) {
        Account account = session.getCard().getAccount();
        account.setBalance(account.getBalance() + amount);
    }

    public void takeCash(TerminalSession session, Double amount) throws NotEnoughMoneyException {
        Account account = session.getCard().getAccount();
        Double newBalance = account.getBalance() - amount;
        if (newBalance < 0) throw new NotEnoughMoneyException();

        account.setBalance(newBalance);
    }
}
