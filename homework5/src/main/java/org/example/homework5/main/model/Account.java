package org.example.homework5.main.model;

import java.util.UUID;

public class Account {

    private final String id;
    private final Client client;
    private Double balance;

    public Account(Client client, Double balance) {
        this.id = UUID.randomUUID().toString();
        this.client = client;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
