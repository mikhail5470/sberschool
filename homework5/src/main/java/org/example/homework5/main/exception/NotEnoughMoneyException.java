package org.example.homework5.main.exception;

public class NotEnoughMoneyException extends Exception {

    public NotEnoughMoneyException() {
        super("try less value");
    }

    public NotEnoughMoneyException(String message) {
        super(message);
    }

    public NotEnoughMoneyException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotEnoughMoneyException(Throwable cause) {
        super(cause);
    }
}
