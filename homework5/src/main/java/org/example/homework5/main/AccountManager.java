package org.example.homework5.main;

import org.example.homework5.main.model.Account;
import org.example.homework5.main.model.Client;

import java.util.ArrayList;

public class AccountManager {

    private final ArrayList<Account> accounts;

    public AccountManager(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public ArrayList<Account> getAccounts() {
        return new ArrayList<>(accounts);
    }

    public ArrayList<Account> getAccounts(Client client) {
        ArrayList<Account> clientAccounts = new ArrayList<>();
        for (Account account : accounts) {
            if (account.getClient() == client) clientAccounts.add(account);
        }

        return clientAccounts;
    }

}
