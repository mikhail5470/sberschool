package org.example.homework5.main.exception;

public class InvalidPinException extends Exception {

    public InvalidPinException() {
        super("try other");
    }

    public InvalidPinException(String message) {
        super(message);
    }

    public InvalidPinException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPinException(Throwable cause) {
        super(cause);
    }
}
