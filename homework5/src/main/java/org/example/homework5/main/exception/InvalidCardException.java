package org.example.homework5.main.exception;

public class InvalidCardException extends Exception {

    public InvalidCardException() {
        super("try other");
    }

    public InvalidCardException(String message) {
        super(message);
    }

    public InvalidCardException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCardException(Throwable cause) {
        super(cause);
    }
}
