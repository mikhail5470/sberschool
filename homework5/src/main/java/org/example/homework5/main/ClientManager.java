package org.example.homework5.main;

import org.example.homework5.main.model.Client;

import java.util.ArrayList;

public class ClientManager {

    private final ArrayList<Client> clients;

    public ClientManager(ArrayList<Client> clients) {
        this.clients = clients;
    }

    public ArrayList<Client> getClients() {
        return new ArrayList<>(clients);
    }

}
