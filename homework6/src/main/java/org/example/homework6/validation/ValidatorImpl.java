package org.example.homework6.validation;

import org.example.homework6.annotations.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;

public class ValidatorImpl<T> implements Validator<T> {

    private HashMap<Class, FieldValidator> fieldsValidators;

    public ValidatorImpl() {
        fieldsValidators = new HashMap<Class, FieldValidator>() {{
            put(NotNull.class, (annotation, value) -> value != null);
            put(Min.class, (annotation, value) -> (int) value >= ((Min) annotation).value());
            put(Max.class, (annotation, value) -> (int) value <= ((Max) annotation).value());
            put(MinLength.class, (annotation, value) -> ((String) value).length() >= ((MinLength) annotation).value());
            put(MaxLength.class, (annotation, value) -> ((String) value).length() <= ((MaxLength) annotation).value());
        }};
    }

    @Override
    public void validate(T o) throws IllegalArgumentException {
        for (Field field : o.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value = null;
            try {
                value = field.get(o);
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
            for (Annotation annotation : field.getAnnotations()) {
                FieldValidator fieldValidator = fieldsValidators.get(annotation.annotationType());
                if (fieldValidator != null && !fieldValidator.validate(annotation, value)) {
                    throw new IllegalArgumentException(String.format("field '%s' validating failed", field.getName()));
                }
            }
        }
    }
}
