package org.example.homework6.validation;

import java.lang.annotation.Annotation;

public interface FieldValidator {

    boolean validate(Annotation annotation, Object value);

}
