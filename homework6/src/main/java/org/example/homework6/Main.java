package org.example.homework6;

import org.example.homework6.validation.Validator;
import org.example.homework6.validation.ValidatorImpl;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Validator<Person> validator = new ValidatorImpl();

        Person person = new Person("Иван", "Петров", "Иванович", new Date(), 20, "123456");
        System.out.println("Person: " + person.toString());
        System.out.println("Validating...");
        validator.validate(person);
        System.out.println("Validated successfully");

        person = new Person("", "Петров", "Иванович", new Date(), 20, "123456");
        System.out.println("Person: " + person.toString());
        System.out.println("Validating...");
        validator.validate(person);
        System.out.println("Validated successfully");
    }
}
