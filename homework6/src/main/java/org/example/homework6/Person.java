package org.example.homework6;

import org.example.homework6.annotations.MaxLength;
import org.example.homework6.annotations.Min;
import org.example.homework6.annotations.MinLength;
import org.example.homework6.annotations.NotNull;

import java.util.Date;

public class Person {

    @MinLength(1)
    @MaxLength(32)
    private String name;

    @MinLength(1)
    @MaxLength(32)
    private String surname;

    @MaxLength(32)
    private String middleName;

    @NotNull
    private Date birthdayDate;

    @Min(1)
    private int age;

    @MinLength(6)
    private String password;

    public Person(String name, String surname, String middleName, Date birthdayDate, int age, String password) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthdayDate = birthdayDate;
        this.age = age;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public int getAge() {
        return age;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthdayDate=" + birthdayDate +
                ", age=" + age +
                ", password='" + password + '\'' +
                '}';
    }
}
