package org.example.homework8.model;

import java.util.Date;

public class Card {

    private final String id;
    private final Account account;
    private final String pin;
    private Date unlockDate;
    private int attempts;

    public Card(String id, Account account, String pin) {
        this.id = id;
        this.account = account;
        this.pin = pin;
        this.unlockDate = new Date(0);
        this.attempts = 0;
    }

    public String getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public String getPin() {
        return pin;
    }

    public Date getUnlockDate() {
        return unlockDate;
    }

    public void setUnlockDate(Date unlockDate) {
        this.unlockDate = unlockDate;
    }

    public boolean isLocked() {
        return unlockDate.after(new Date());
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }
}
