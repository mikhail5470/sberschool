package org.example.homework8.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class CardTest {

    private Card card;

    @Before
    public void setUp() throws Exception {
        Client client = new Client(new PersonName("1", "2", "3"));
        Account account = new Account(client, 1d);
        card = new Card("1", account, "1234");
    }

    @Test
    public void isLocked_setUnlockDate_past() {
        card.setUnlockDate(new Date(System.currentTimeMillis() - 1));
        Assert.assertFalse(card.isLocked());
    }

    @Test
    public void isLocked_setUnlockDate_future() {
        card.setUnlockDate(new Date(System.currentTimeMillis() + 1));
        Assert.assertTrue(card.isLocked());
    }
}