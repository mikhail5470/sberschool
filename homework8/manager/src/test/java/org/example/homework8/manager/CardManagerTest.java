package org.example.homework8.manager;

import org.example.homework8.model.Account;
import org.example.homework8.model.Card;
import org.example.homework8.model.Client;
import org.example.homework8.model.PersonName;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CardManagerTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getCards_byAccount() {
        Client client = new Client(new PersonName("1", "2", "3"));
        Account account1 = new Account(client, 1d);
        Account account2 = new Account(client, 1d);
        Card card1 = new Card("1", account1, "1234");
        Card card2 = new Card("2", account2, "1234");
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(card1);
        cards.add(card2);
        CardManager cardManager = new CardManager(cards);

        List<Card> account1Cards = cardManager.getCards(account1);
        Assert.assertTrue(account1Cards.size() == 1 && card1 == account1Cards.get(0));

        List<Card> account2Cards = cardManager.getCards(account2);
        Assert.assertTrue(account2Cards.size() == 1 && card2 == account2Cards.get(0));
    }
}