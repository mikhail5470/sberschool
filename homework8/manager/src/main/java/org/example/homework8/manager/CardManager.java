package org.example.homework8.manager;

import org.example.homework8.model.Account;
import org.example.homework8.model.Card;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class CardManager {

    private final List<Card> cards;

    public CardManager(List<Card> cards) {
        this.cards = new ArrayList<>(cards);
    }

    public List<Card> getCards() {
        return new ArrayList<>(cards);
    }

    public Card getCard(String id) {
        for (Card card : cards) {
            if (card.getId().equals(id)) return card;
        }
        return null;
    }

    public List<Card> getCards(Account account) {
        ArrayList<Card> accountCards = new ArrayList<>();
        for (Card card : cards) {
            if (card.getAccount() == account) accountCards.add(card);
        }
        return accountCards;
    }

}
