package org.example.homework8.manager;

import org.example.homework8.model.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientManager {

    private final List<Client> clients;

    public ClientManager(List<Client> clients) {
        this.clients = new ArrayList<>(clients);
    }

    public List<Client> getClients() {
        return new ArrayList<>(clients);
    }

}
