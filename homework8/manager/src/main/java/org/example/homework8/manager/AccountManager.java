package org.example.homework8.manager;

import org.example.homework8.model.Account;
import org.example.homework8.model.Client;

import java.util.ArrayList;
import java.util.List;

public class AccountManager {

    private final List<Account> accounts;

    public AccountManager(List<Account> accounts) {
        this.accounts = new ArrayList<>(accounts);
    }

    public List<Account> getAccounts() {
        return new ArrayList<>(accounts);
    }

    public List<Account> getAccounts(Client client) {
        ArrayList<Account> clientAccounts = new ArrayList<>();
        for (Account account : accounts) {
            if (account.getClient() == client) clientAccounts.add(account);
        }

        return clientAccounts;
    }

}
