package org.example.homework8.terminal.server.exception;

import java.util.Date;

public class CardIsLockedException extends Exception {

    public CardIsLockedException() {
        super("try later");
    }

    public CardIsLockedException(Date date) {
        super("wait to " + date.toString());
    }

    public CardIsLockedException(String message) {
        super(message);
    }

    public CardIsLockedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CardIsLockedException(Throwable cause) {
        super(cause);
    }
}
