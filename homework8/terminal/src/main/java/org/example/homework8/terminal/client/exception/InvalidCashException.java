package org.example.homework8.terminal.client.exception;

public class InvalidCashException extends Exception {

    public InvalidCashException() {
        super("cash must be a multiple of 100 and be positive");
    }

    public InvalidCashException(String message) {
        super(message);
    }

    public InvalidCashException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCashException(Throwable cause) {
        super(cause);
    }
}
