package org.example.homework8.terminal.client;

import org.example.homework8.terminal.server.exception.CardIsLockedException;
import org.example.homework8.terminal.server.exception.InvalidCardException;
import org.example.homework8.terminal.server.exception.InvalidPinException;
import org.example.homework8.terminal.server.exception.NotEnoughMoneyException;
import org.example.homework8.terminal.client.exception.AlreadyAuthorizedException;
import org.example.homework8.terminal.client.exception.InvalidCashException;
import org.example.homework8.terminal.client.exception.NotAuthorizedException;

public interface Terminal {

    void run();
    void authorize(String cardID, String pin) throws CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException;
    Double getBalance() throws NotAuthorizedException;
    void putCash(Double amount) throws NotAuthorizedException, InvalidCashException;
    void takeCash(Double amount) throws NotAuthorizedException, InvalidCashException, NotEnoughMoneyException;
    void exit() throws NotAuthorizedException;

}
