package org.example.homework8;

import org.example.homework8.manager.AccountManager;
import org.example.homework8.manager.CardManager;
import org.example.homework8.manager.ClientManager;
import org.example.homework8.model.Account;
import org.example.homework8.model.Card;
import org.example.homework8.model.Client;
import org.example.homework8.model.PersonName;
import org.example.homework8.terminal.server.TerminalServer;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Client client1 = new Client(new PersonName("John", "test", "test"));
        ArrayList<Client> clients = new ArrayList<>();
        clients.add(client1);

        Account acc1 = new Account(client1, 5_000_000d);
        Account acc2 = new Account(client1, 0d);
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(acc1);

        ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card("1", acc1, "1234"));
        cards.add(new Card("2", acc2, "0000"));

        System.out.println("available cards:");
        for (Card card : cards) {
            System.out.println(card.getId() + ": " + card.getPin());
        }

        TerminalServer terminalServer = new TerminalServer(
                new ClientManager(clients),
                new AccountManager(accounts),
                new CardManager(cards)
        );

        terminalServer.run();

    }
}
