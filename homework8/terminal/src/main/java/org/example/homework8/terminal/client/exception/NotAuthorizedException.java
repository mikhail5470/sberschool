package org.example.homework8.terminal.client.exception;

public class NotAuthorizedException extends Exception {

    public NotAuthorizedException() {
        super("authorize first");
    }

    public NotAuthorizedException(String message) {
        super(message);
    }

    public NotAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAuthorizedException(Throwable cause) {
        super(cause);
    }
}
