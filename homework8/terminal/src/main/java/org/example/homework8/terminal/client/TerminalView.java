package org.example.homework8.terminal.client;

public interface TerminalView {

    void run();
    void outputMessage(String message);

}
