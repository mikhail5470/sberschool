package org.example.homework8.terminal.server;

import org.example.homework8.manager.AccountManager;
import org.example.homework8.manager.CardManager;
import org.example.homework8.manager.ClientManager;
import org.example.homework8.model.Card;
import org.example.homework8.terminal.client.Terminal;
import org.example.homework8.terminal.client.TerminalImpl;
import org.example.homework8.terminal.server.exception.CardIsLockedException;
import org.example.homework8.terminal.server.exception.InvalidCardException;
import org.example.homework8.terminal.server.exception.InvalidPinException;
import org.example.homework8.terminal.server.exception.NotEnoughMoneyException;
import org.example.homework8.model.Account;

import java.time.Duration;
import java.util.Date;

public class TerminalServer {

    private static final int LOCK_CARD_ATTEMPTS = 3;
    private static final Duration LOCK_CARD_DURATION = Duration.ofSeconds(5L);

    private final ClientManager clientManager;
    private final AccountManager accountManager;
    private final CardManager cardManager;
    private final Terminal terminal;

    public TerminalServer(ClientManager clientManager, AccountManager accountManager, CardManager cardManager) {
        this.clientManager = clientManager;
        this.accountManager = accountManager;
        this.cardManager = cardManager;
        this.terminal = new TerminalImpl(this);
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void run() {
        terminal.run();
    }

    public TerminalSession authorize(String cardID, String pin) throws InvalidCardException, InvalidPinException, CardIsLockedException {
        Card card = cardManager.getCard(cardID);
        if (card == null) throw new InvalidCardException();
        if (card.isLocked()) throw new CardIsLockedException(card.getUnlockDate());
        if (!card.getPin().equals(pin)) {
            int attempts = card.getAttempts() + 1;
            card.setAttempts(attempts);
            if (attempts >= LOCK_CARD_ATTEMPTS) {
                card.setUnlockDate(new Date(System.currentTimeMillis() + LOCK_CARD_DURATION.toMillis()));
            }
            throw new InvalidPinException();
        }

        card.setAttempts(0);

        return new TerminalSession(card);
    }

    public Double getBalance(TerminalSession session) {
        return session.getCard().getAccount().getBalance();
    }

    public void addCash(TerminalSession session, Double amount) {
        Account account = session.getCard().getAccount();
        account.setBalance(account.getBalance() + amount);
    }

    public void takeCash(TerminalSession session, Double amount) throws NotEnoughMoneyException {
        Account account = session.getCard().getAccount();
        Double newBalance = account.getBalance() - amount;
        if (newBalance < 0) throw new NotEnoughMoneyException();

        account.setBalance(newBalance);
    }
}
