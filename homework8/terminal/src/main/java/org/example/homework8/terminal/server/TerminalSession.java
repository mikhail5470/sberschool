package org.example.homework8.terminal.server;

import org.example.homework8.model.Card;

public class TerminalSession {

    private final Card card;

    public TerminalSession(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }
}
