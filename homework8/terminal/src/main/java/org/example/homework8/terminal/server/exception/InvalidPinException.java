package org.example.homework8.terminal.server.exception;

public class InvalidPinException extends Exception {

    public InvalidPinException() {
        super("try other");
    }

    public InvalidPinException(String message) {
        super(message);
    }

    public InvalidPinException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPinException(Throwable cause) {
        super(cause);
    }
}
