package org.example.homework8.terminal.client;

import org.example.homework8.terminal.server.exception.CardIsLockedException;
import org.example.homework8.terminal.server.exception.InvalidCardException;
import org.example.homework8.terminal.server.exception.InvalidPinException;
import org.example.homework8.terminal.server.exception.NotEnoughMoneyException;
import org.example.homework8.terminal.client.exception.AlreadyAuthorizedException;
import org.example.homework8.terminal.client.exception.InvalidCashException;
import org.example.homework8.terminal.client.exception.NotAuthorizedException;

import java.util.Scanner;

public class ConsoleTerminalView implements TerminalView {

    private Terminal terminal;
    private Scanner scanner;

    public ConsoleTerminalView(Terminal terminal) {
        this.terminal = terminal;
        this.scanner = new Scanner(System.in);
        welcome();
    }

    private void welcome() {
        outputMessage("Hello!");
        outputMessage("Available commands:");
        outputMessage("card <ID> <PIN>");
        outputMessage("balance");
        outputMessage("put <cash>");
        outputMessage("take <cash>");
        outputMessage("exit");
    }

    public void run() {
        while (scanner.hasNextLine()) {
            String[] args = scanner.nextLine().split(" ");
            switch (args[0]) {
                case "card":
                    if (args.length < 3) {
                        outputMessage("invalid command format");
                        break;
                    }

                    try {
                        terminal.authorize(args[1], args[2]);
                        outputMessage("you are successfully authorized");
                    } catch (CardIsLockedException e) {
                        outputMessage(String.format("account is locked (%s)", e.getMessage()));
                    } catch (AlreadyAuthorizedException e) {
                        outputMessage(String.format("you are already authorized (%s)", e.getMessage()));
                    } catch (InvalidCardException e) {
                        outputMessage(String.format("invalid card (%s)", e.getMessage()));
                    } catch (InvalidPinException e) {
                        outputMessage(String.format("invalid PIN (%s)", e.getMessage()));
                    }
                    break;

                case "balance":
                    try {
                        outputMessage("balance is " + terminal.getBalance().toString());
                    } catch (NotAuthorizedException e) {
                        outputMessage(String.format("you are not authorized (%s)", e.getMessage()));
                    }
                    break;

                case "put":
                    if (args.length < 2) {
                        outputMessage("invalid command format");
                        break;
                    }

                    try {
                        Double amount = Double.valueOf(args[1]);
                        terminal.putCash(amount);
                        outputMessage("successfully put " + amount.toString());
                    } catch (NumberFormatException e) {
                        outputMessage("invalid format");
                    } catch (NotAuthorizedException e) {
                        outputMessage(String.format("you are not authorized (%s)", e.getMessage()));
                    } catch (InvalidCashException e) {
                        outputMessage(String.format("invalid cash value (%s)", e.getMessage()));
                    }
                    break;

                case "take":
                    if (args.length < 2) {
                        outputMessage("invalid command format");
                        break;
                    }

                    try {
                        Double amount = Double.valueOf(args[1]);
                        terminal.takeCash(amount);
                        outputMessage("successfully taken " + amount.toString());
                    } catch (NumberFormatException e) {
                        outputMessage("invalid format");
                    } catch (NotAuthorizedException e) {
                        outputMessage(String.format("you are not authorized (%s)", e.getMessage()));
                    } catch (InvalidCashException e) {
                        outputMessage(String.format("invalid cash value (%s)", e.getMessage()));
                    } catch (NotEnoughMoneyException e) {
                        outputMessage(String.format("not enough money (%s)", e.getMessage()));
                    }
                    break;

                case "exit":
                    try {
                        terminal.exit();
                        outputMessage("successfully exit");
                    } catch (NotAuthorizedException e) {
                        outputMessage(String.format("you are not authorized (%s)", e.getMessage()));
                    }

            }
        }
    }

    @Override
    public void outputMessage(String message) {
        System.out.println(message);
    }
}
