package org.example.homework8.terminal.client;

import org.example.homework8.terminal.server.exception.CardIsLockedException;
import org.example.homework8.terminal.server.exception.InvalidCardException;
import org.example.homework8.terminal.server.exception.InvalidPinException;
import org.example.homework8.terminal.server.exception.NotEnoughMoneyException;
import org.example.homework8.terminal.client.exception.AlreadyAuthorizedException;
import org.example.homework8.terminal.client.exception.InvalidCashException;
import org.example.homework8.terminal.client.exception.NotAuthorizedException;
import org.example.homework8.terminal.server.TerminalServer;
import org.example.homework8.terminal.server.TerminalSession;

public class TerminalImpl implements Terminal {

    private final TerminalServer server;
    private TerminalView view;
    private TerminalSession session;

    public TerminalImpl(TerminalServer server) {
        this.server = server;
        this.view = new ConsoleTerminalView(this);
        this.session = null;
    }

    @Override
    public void run() {
        view.run();
    }

    @Override
    public void authorize(String cardID, String pin) throws AlreadyAuthorizedException, InvalidCardException, InvalidPinException, CardIsLockedException {
        if (session != null) throw new AlreadyAuthorizedException();

        session = server.authorize(cardID, pin);
    }

    @Override
    public Double getBalance() throws NotAuthorizedException {
        if (session == null) throw new NotAuthorizedException();

        return server.getBalance(session);
    }

    @Override
    public void putCash(Double amount) throws NotAuthorizedException, InvalidCashException {
        if (session == null) throw new NotAuthorizedException();
        if ((amount < 1) || (amount % 100D != 0)) throw new InvalidCashException();

        server.addCash(session, amount);
    }

    @Override
    public void takeCash(Double amount) throws NotAuthorizedException, InvalidCashException, NotEnoughMoneyException {
        if (session == null) throw new NotAuthorizedException();
        if ((amount < 1) || (amount % 100D != 0)) throw new InvalidCashException();

        server.takeCash(session, amount);
    }

    @Override
    public void exit() throws NotAuthorizedException {
        if (session == null) throw new NotAuthorizedException();

        session = null;
    }
}
