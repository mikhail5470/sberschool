package org.example.homework8.terminal.client;

import org.example.homework8.manager.AccountManager;
import org.example.homework8.manager.CardManager;
import org.example.homework8.manager.ClientManager;
import org.example.homework8.model.Account;
import org.example.homework8.model.Card;
import org.example.homework8.model.Client;
import org.example.homework8.model.PersonName;
import org.example.homework8.terminal.client.exception.AlreadyAuthorizedException;
import org.example.homework8.terminal.client.exception.InvalidCashException;
import org.example.homework8.terminal.client.exception.NotAuthorizedException;
import org.example.homework8.terminal.server.TerminalServer;
import org.example.homework8.terminal.server.exception.CardIsLockedException;
import org.example.homework8.terminal.server.exception.InvalidCardException;
import org.example.homework8.terminal.server.exception.InvalidPinException;
import org.example.homework8.terminal.server.exception.NotEnoughMoneyException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class TerminalImplTest {

    private Client client;
    private Account account;
    private Card card;
    private ClientManager clientManager;
    private AccountManager accountManager;
    private CardManager cardManager;
    private TerminalServer terminalServer;
    private Terminal terminal;

    @Before
    public void setUp() throws Exception {
        client = new Client(new PersonName("1", "2", "3"));
        account = new Account(client, 1d);
        card = new Card("1", account, "1234");
        clientManager = new ClientManager(Arrays.asList(client));
        accountManager = new AccountManager(Arrays.asList(account));
        cardManager = new CardManager(Arrays.asList(card));
        terminalServer = new TerminalServer(clientManager, accountManager, cardManager);
        terminal = terminalServer.getTerminal();
    }


    @Test
    public void authorize_noExceptions() throws CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException {
        terminal.authorize("1", "1234");
    }

    @Test(expected = AlreadyAuthorizedException.class)
    public void authorize_AlreadyAuthorizedException() throws CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException {
        terminal.authorize("1", "1234");
        terminal.authorize("1", "1234");
    }

    @Test(expected = NotAuthorizedException.class)
    public void getBalance_NotAuthorizedException() throws NotAuthorizedException {
        terminal.getBalance();
    }

    @Test
    public void getBalance_equal() throws NotAuthorizedException, CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException {
        terminal.authorize("1", "1234");
        Assert.assertTrue(terminal.getBalance() == account.getBalance());
    }

    @Test
    public void putCash_noExceptions() throws NotAuthorizedException, InvalidCashException, CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException {
        terminal.authorize("1", "1234");
        terminal.putCash(100d);
    }

    @Test(expected = NotAuthorizedException.class)
    public void putCash_NotAuthorizedException() throws NotAuthorizedException, InvalidCashException {
        terminal.putCash(0d);
    }

    @Test(expected = InvalidCashException.class)
    public void putCash_InvalidCashException() throws NotAuthorizedException, InvalidCashException, CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException {
        terminal.authorize("1", "1234");
        terminal.putCash(99d);
    }

    @Test
    public void takeCash_noExceptions() throws NotAuthorizedException, InvalidCashException, CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException, NotEnoughMoneyException {
        account.setBalance(100d);
        terminal.authorize("1", "1234");
        terminal.takeCash(100d);
    }

    @Test(expected = NotAuthorizedException.class)
    public void takeCash_NotAuthorizedException() throws NotAuthorizedException, InvalidCashException, NotEnoughMoneyException {
        terminal.takeCash(1d);
    }

    @Test(expected = InvalidCashException.class)
    public void takeCash_InvalidCashException() throws NotAuthorizedException, InvalidCashException, CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException, NotEnoughMoneyException {
        terminal.authorize("1", "1234");
        terminal.takeCash(1d);
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void takeCash_NotEnoughMoneyException() throws NotAuthorizedException, InvalidCashException, CardIsLockedException, AlreadyAuthorizedException, InvalidCardException, InvalidPinException, NotEnoughMoneyException {
        account.setBalance(100d);
        terminal.authorize("1", "1234");
        terminal.takeCash(200d);
    }
}