package org.example.homework8.terminal.server;

import org.example.homework8.manager.AccountManager;
import org.example.homework8.manager.CardManager;
import org.example.homework8.manager.ClientManager;
import org.example.homework8.model.Account;
import org.example.homework8.model.Card;
import org.example.homework8.model.Client;
import org.example.homework8.model.PersonName;
import org.example.homework8.terminal.server.exception.CardIsLockedException;
import org.example.homework8.terminal.server.exception.InvalidCardException;
import org.example.homework8.terminal.server.exception.InvalidPinException;
import org.example.homework8.terminal.server.exception.NotEnoughMoneyException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.*;

public class TerminalServerTest {

    private Client client;
    private Account account;
    private Card card;
    private ClientManager clientManager;
    private AccountManager accountManager;
    private CardManager cardManager;
    private TerminalServer terminalServer;

    @Before
    public void setUp() throws Exception {
        client = new Client(new PersonName("1", "2", "3"));
        account = new Account(client, 1d);
        card = new Card("1", account, "1234");
        clientManager = new ClientManager(Arrays.asList(client));
        accountManager = new AccountManager(Arrays.asList(account));
        cardManager = new CardManager(Arrays.asList(card));
        terminalServer = new TerminalServer(clientManager, accountManager, cardManager);
    }

    @Test
    public void authorize_noExceptions() throws InvalidCardException, InvalidPinException, CardIsLockedException {
        terminalServer.authorize("1", "1234");
    }

    @Test(expected = InvalidCardException.class)
    public void authorize_InvalidCardException() throws InvalidCardException, InvalidPinException, CardIsLockedException {
        terminalServer.authorize("2", "1234");
    }

    @Test(expected = InvalidPinException.class)
    public void authorize_InvalidPinException() throws InvalidCardException, InvalidPinException, CardIsLockedException {
        terminalServer.authorize("1", "1235");
    }

    @Test(expected = CardIsLockedException.class)
    public void authorize_CardIsLockedException() throws InvalidCardException, InvalidPinException, CardIsLockedException {
        card.setUnlockDate(new Date(System.currentTimeMillis() + 1));
        terminalServer.authorize("1", "1234");
    }

    @Test
    public void authorize_TerminalSession() throws InvalidCardException, InvalidPinException, CardIsLockedException {
        TerminalSession terminalSession = terminalServer.authorize("1", "1234");
        Assert.assertTrue(terminalSession.getCard() == card);
    }

    @Test
    public void takeCash_noExceptions() throws InvalidCardException, InvalidPinException, CardIsLockedException, NotEnoughMoneyException {
        TerminalSession terminalSession = terminalServer.authorize("1", "1234");
        terminalServer.takeCash(terminalSession, 1d);
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void takeCash_NotEnoughMoneyException() throws InvalidCardException, InvalidPinException, CardIsLockedException, NotEnoughMoneyException {
        TerminalSession terminalSession = terminalServer.authorize("1", "1234");
        terminalServer.takeCash(terminalSession, 2d);
    }
}