package org.example.homework2.transfer;

import org.example.homework2.account.Account;

public class AccountTransfer extends AbstractTransfer{

    private Account from;
    private Account to;
    private double cash;

    public AccountTransfer(Account from, Account to, double cash) {
        this.from = from;
        this.to = to;
        this.cash = cash;
    }

    public Account getFrom() {
        return from;
    }

    public Account getTo() {
        return to;
    }

    public double getCash() {
        return cash;
    }

    @Override
    public void execute() {
        from.take(cash);
        to.give(cash);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountTransfer that = (AccountTransfer) o;

        if (Double.compare(that.cash, cash) != 0) return false;
        if (!from.equals(that.from)) return false;
        return to.equals(that.to);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = from.hashCode();
        result = 31 * result + to.hashCode();
        temp = Double.doubleToLongBits(cash);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

}
