package org.example.homework2.transfer;

public interface Transfer {

    void execute();

}
