package org.example.homework2.client;

public class Client {

    private Passport passport;

    public Client(Passport passport) {
        this.passport = passport;
    }

    public Passport getPassport() {
        return passport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        return passport.equals(client.passport);
    }

    @Override
    public int hashCode() {
        return passport.hashCode();
    }

    @Override
    public String toString() {
        return "{" +
            "\"passport\":\"" + passport.toString() + "\"" +
        "}";
    }

}
