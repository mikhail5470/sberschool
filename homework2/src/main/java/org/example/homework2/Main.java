package org.example.homework2;

import org.example.homework2.account.Account;
import org.example.homework2.account.DebitAccount;
import org.example.homework2.client.Client;
import org.example.homework2.client.Passport;
import org.example.homework2.client.PersonName;
import org.example.homework2.credit.Credit;
import org.example.homework2.credit.manager.CreditManager;
import org.example.homework2.credit.manager.MainCreditManager;
import org.example.homework2.credit.service.CreditService;
import org.example.homework2.credit.service.MainCreditService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Client cl1 = new Client(new Passport("457345", new PersonName("Иван", "Петров", "Иванович")));
        Client cl2 = new Client(new Passport("356547", new PersonName("Борис", "Иванов", "Михайлович")));

        Account acc1 = new DebitAccount(cl1, 90_000_000d);
        Account acc2 = new DebitAccount(cl2, 50_000_000d);

        List<Credit> credits = new ArrayList<>();
        credits.add(new Credit(cl1, 5_000_000d));
        credits.add(new Credit(cl1, 200_000d));
        credits.add(new Credit(cl2, 2_500_000d));
        credits.add(new Credit(cl2, 50_000d));
        credits.add(new Credit(cl2, 20_000d));

        CreditManager creditManager = new MainCreditManager(credits);
        CreditService creditService = new MainCreditService(creditManager);

        System.out.println("before:");
        for (Credit credit : credits) {
            System.out.println(credit);
        }

        for (Credit credit : creditManager.getClientCredits(cl1)) {
            creditService.repay(acc1, credit, 50_000d);
        }
        for (Credit credit : creditManager.getClientCredits(cl2)) {
            creditService.repay(acc2, credit, 20_000d);
        }

        System.out.println("after:");
        for (Credit credit : credits) {
            System.out.println(credit);
        }

    }
}
