package org.example.homework2.credit.service;

import org.example.homework2.account.Account;
import org.example.homework2.credit.Credit;
import org.example.homework2.credit.manager.CreditManager;

public class MainCreditService extends AbstractCreditService {

    private CreditManager creditManager;

    public MainCreditService(CreditManager creditManager) {
        this.creditManager = creditManager;
    }

    @Override
    public void repay(Account account, Credit credit, double cash) {
        account.take(cash);
        credit.repay(cash);
    }

}
