package org.example.homework2.credit.manager;

import org.example.homework2.client.Client;
import org.example.homework2.credit.Credit;

import java.util.List;

public interface CreditManager {

    List<Credit> getCredits();
    List<Credit> getClientCredits(Client client);

}
