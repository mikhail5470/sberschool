package org.example.homework2.credit.manager;

import org.example.homework2.client.Client;
import org.example.homework2.credit.Credit;

import java.util.ArrayList;
import java.util.List;

public class MainCreditManager extends AbstractCreditManager {

    private List<Credit> credits;

    public MainCreditManager(List<Credit> credits) {
        this.credits = credits;
    }

    @Override
    public List<Credit> getCredits() {
        return credits;
    }

    @Override
    public List<Credit> getClientCredits(Client client) {

        List<Credit> clientCredits = new ArrayList<>();
        for (Credit credit : credits) {
            if (client == credit.getClient())
                clientCredits.add(credit);
        }

        return clientCredits;
    }
}
