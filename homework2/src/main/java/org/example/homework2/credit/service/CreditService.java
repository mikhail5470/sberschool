package org.example.homework2.credit.service;

import org.example.homework2.account.Account;
import org.example.homework2.account.DebitAccount;
import org.example.homework2.credit.Credit;

public interface CreditService {

    void repay(Account account, Credit credit, double cash);

}
