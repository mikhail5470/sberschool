package org.example.homework2.account;

import org.example.homework2.client.Client;

public class DebitAccount extends AbstractAccount {

    private Client client;
    private double cash;

    public DebitAccount(Client client, double cash) {
        this.client = client;
        this.cash = cash;
    }

    public Client getClient() {
        return client;
    }

    public double getCash() {
        return cash;
    }

    @Override
    public void take(double amount) {
        cash -= amount;
    }

    @Override
    public void give(double amount) {
        cash += amount;
    }

}
