package org.example.homework2.account;

public interface Account {

    void take(double cash);
    void give(double cash);

}
