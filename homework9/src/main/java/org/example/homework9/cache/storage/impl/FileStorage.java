package org.example.homework9.cache.storage.impl;

import org.example.homework9.cache.storage.AbstractStorage;

import java.io.*;
import java.util.List;
import java.util.Objects;

public class FileStorage extends AbstractStorage {

    private static final String FILE_NAME_POSTFIX = ".data";

    public FileStorage(String catalog) {
        super(catalog);
    }

    @Override
    public void set(Object object, String key, List<Object> args, Object value) throws IOException {
        Integer hash = Objects.hash(object, key, args);

        File file = new File(getCatalog(), hash + FILE_NAME_POSTFIX);
        file.getParentFile().mkdirs();
        file.createNewFile();

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(value);
        }
    }

    @Override
    public Object get(Object object, String key, List<Object> args) throws IOException, ClassNotFoundException {
        Integer hash = Objects.hash(object, key, args);

        File file = new File(getCatalog(), hash + FILE_NAME_POSTFIX);
        if (!file.exists()) return null;

        Object value = null;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            value = ois.readObject();
        }

        return value;
    }
}
