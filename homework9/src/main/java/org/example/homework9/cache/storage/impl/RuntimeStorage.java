package org.example.homework9.cache.storage.impl;

import org.example.homework9.cache.storage.AbstractStorage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class RuntimeStorage extends AbstractStorage {

    private Map<Integer, Object> data;

    public RuntimeStorage(String catalog) {
        super(catalog);
        data = new HashMap<>();
    }

    @Override
    public void set(Object object, String key, List<Object> args, Object value) {
        data.put(Objects.hash(object, key, args), value);
    }

    @Override
    public Object get(Object object, String key, List<Object> args) {
        return data.get(Objects.hash(object, key, args));
    }
}
