package org.example.homework9.cache.storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface Storage {

    void set(Object object, String key, List<Object> args, Object value) throws IOException;
    Object get(Object object, String key, List<Object> args) throws IOException, ClassNotFoundException;

}
