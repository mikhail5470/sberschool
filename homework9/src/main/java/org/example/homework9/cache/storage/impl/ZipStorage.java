package org.example.homework9.cache.storage.impl;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipStorage extends FileStorage {

    private static final String FILE_NAME_POSTFIX = ".zip";

    public ZipStorage(String catalog) {
        super(catalog);
    }

    @Override
    public void set(Object object, String key, List<Object> args, Object value) throws IOException {
        Integer hash = Objects.hash(object, key, args);

        File file = new File(getCatalog(), hash + FILE_NAME_POSTFIX);
        file.getParentFile().mkdirs();
        file.createNewFile();

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(file))) {
            zos.putNextEntry(new ZipEntry("data"));

            try(ObjectOutputStream oos = new ObjectOutputStream(zos)) {
                oos.writeObject(value);
            }
        }
    }

    @Override
    public Object get(Object object, String key, List<Object> args) throws IOException, ClassNotFoundException {
        Integer hash = Objects.hash(object, key, args);

        File file = new File(getCatalog(), hash + FILE_NAME_POSTFIX);
        if (!file.exists()) return null;

        Object value = null;
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(file))) {
            zis.getNextEntry();

            try (ObjectInputStream ois = new ObjectInputStream(zis)) {
                value = ois.readObject();
            }
        }

        return value;
    }
}
