package org.example.homework9.cache.annotation;

import org.example.homework9.cache.CacheProxy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cache {
    CacheProxy.StorageType storage() default CacheProxy.StorageType.FILE;
    String key() default "";
    int resultListLimit() default -1;
}
