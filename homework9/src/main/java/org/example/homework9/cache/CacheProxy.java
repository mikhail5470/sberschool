package org.example.homework9.cache;

import org.example.homework9.cache.annotation.Cache;
import org.example.homework9.cache.annotation.Ignore;
import org.example.homework9.cache.storage.impl.FileStorage;
import org.example.homework9.cache.storage.impl.RuntimeStorage;
import org.example.homework9.cache.storage.Storage;
import org.example.homework9.cache.storage.impl.ZipStorage;

import java.io.NotSerializableException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;

public class CacheProxy {

    public enum StorageType {
        RUNTIME,
        FILE,
        ZIP
    }

    private class CacheInvocationHandler implements InvocationHandler {

        private Object object;

        public CacheInvocationHandler(Object object) {
            this.object = object;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Cache cacheAnnotation = method.getDeclaredAnnotation(Cache.class);
            if (cacheAnnotation == null) return method.invoke(object, args);

            ArrayList<Object> actualArgs = new ArrayList<>();
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            for (int i = 0; args != null && i < args.length; i++) {
                boolean ignore = false;
                for (Annotation parameterAnnotation : parameterAnnotations[i]) {
                    if (parameterAnnotation.annotationType().equals(Ignore.class)) {
                        ignore = true;
                        break;
                    }
                }
                if (!ignore) actualArgs.add(args[i]);
            }

            String key = cacheAnnotation.key();
            if (key.equals("")) key = method.getName();

            Storage storage = storages.get(cacheAnnotation.storage());
            Object value = storage.get(object, key, actualArgs);
            if (value != null) {
                System.out.println(String.format("GET from storage %s %s %s %s", cacheAnnotation.storage(), key, actualArgs, value));
                return value;
            }

            value = method.invoke(object, args);
            if (!(value instanceof Serializable))
                throw new NotSerializableException("return type must be Serializable");

            Integer listLimit = cacheAnnotation.resultListLimit();
            if (method.getReturnType().isAssignableFrom(List.class) && listLimit > -1) {
                List listValue = (List) value;
                while (listValue.size() > listLimit) {
                    listValue.remove(listValue.size() - 1);
                }
                value = listValue;
            }

            storage.set(object, key, actualArgs, value);
            System.out.println(String.format("WRITE to storage %s %s %s %s", cacheAnnotation.storage(), key, actualArgs, value));

            return value;
        }
    }

    private String catalog;
    private Map<StorageType, Storage> storages;

    public CacheProxy(String catalog) {
        this.catalog = catalog;

        Map<StorageType, Storage> storages = new HashMap<>();
        storages.put(StorageType.FILE, new FileStorage(catalog));
        storages.put(StorageType.ZIP, new ZipStorage(catalog));
        storages.put(StorageType.RUNTIME, new RuntimeStorage(catalog));
        this.storages = storages;
    }

    public <T> T cache(T service) {
        return
                (T) Proxy.newProxyInstance(
                        service.getClass().getClassLoader(),
                        service.getClass().getInterfaces(),
                        new CacheInvocationHandler(service)
                );
    }

}
