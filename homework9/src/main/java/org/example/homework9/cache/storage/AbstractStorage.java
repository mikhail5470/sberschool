package org.example.homework9.cache.storage;

public abstract class AbstractStorage implements Storage {

    private final String catalog;

    public AbstractStorage(String catalog) {
        this.catalog = catalog;
    }

    public String getCatalog() {
        return catalog;
    }
}
