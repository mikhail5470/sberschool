package org.example.homework9;

import org.example.homework9.cache.CacheProxy;
import org.example.homework9.service.Service;
import org.example.homework9.service.SomeService;

import java.util.Date;

public class Main {
    public static void main(String[] args) {

        CacheProxy cacheProxy = new CacheProxy("some");

        Service someService = new SomeService();
        Service cachedService = cacheProxy.cache(someService);

        cachedService.work("test", 1d, new Date());
        cachedService.work("test", 1d, new Date(System.currentTimeMillis() + 1000));
        cachedService.work("test2", 1d, new Date());
        cachedService.work("test2", 1d, new Date());

        cachedService.work("test", 5d);
        cachedService.work("test", 5d);
        cachedService.work("test2", 5d);

        cachedService.work("test");
        cachedService.work("test");
        cachedService.work("test2");

        cachedService.work(5d);
        cachedService.work(5d);
        cachedService.work(6d);

        cachedService.other();
        cachedService.other();

        cachedService.bad();

    }
}
