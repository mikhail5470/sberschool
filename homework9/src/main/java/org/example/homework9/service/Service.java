package org.example.homework9.service;

import org.example.homework9.cache.annotation.Cache;
import org.example.homework9.cache.CacheProxy;
import org.example.homework9.cache.annotation.Ignore;

import java.util.Date;
import java.util.List;

public interface Service {

    @Cache(
            storage = CacheProxy.StorageType.FILE,
            key = "workByDoubleArg",
            resultListLimit = 2
    )
    List<String> work(String item, double value, @Ignore Date date);

    @Cache(
            storage = CacheProxy.StorageType.ZIP,
            key = "workByDoubleArg",
            resultListLimit = 4
    )
    List<String> work(String item, double value);

    @Cache(
            storage = CacheProxy.StorageType.RUNTIME
    )
    List<String> work(String item);

    @Cache
    boolean work(double value);

    // no cache
    void other();

    @Cache
    Service bad();

}
