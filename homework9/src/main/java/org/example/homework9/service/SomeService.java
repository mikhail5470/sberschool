package org.example.homework9.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SomeService implements Service {
    @Override
    public List<String> work(String item, double value, Date date) {
        return new ArrayList<>(Arrays.asList("list1", "res1", "res2", "res3", "res4", "res5"));
    }

    @Override
    public List<String> work(String item, double value) {
        return new ArrayList<>(Arrays.asList("list2", "res1", "res2", "res3", "res4", "res5"));
    }

    @Override
    public List<String> work(String item) {
        return new ArrayList<>(Arrays.asList("list3", "res1", "res2", "res3", "res4", "res5"));
    }

    @Override
    public boolean work(double value) {
        return false;
    }

    @Override
    public void other() {
    }

    @Override
    public Service bad() {
        return new SomeService();
    }
}
