CREATE TABLE dishes (
    id   INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT    NOT NULL
                 UNIQUE
);

CREATE TABLE ingredients (
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    dish_id INTEGER REFERENCES dishes (id)
                    NOT NULL,
    name    TEXT    NOT NULL,
    mass    DOUBLE  NOT NULL
                    DEFAULT (0)
);