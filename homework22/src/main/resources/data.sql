INSERT INTO dishes (name) VALUES
    ('dish1'),
    ('dish2'),
    ('dish3'),
    ('dish4');

INSERT INTO ingredients (dish_id, name, mass) VALUES
    (1, 'ing1', 0.1),
    (1, 'ing2', 0.6),
    (2, 'ing1', 2.5);
