package org.example.homework22;

import org.example.homework22.dao.model.DishDao;
import org.example.homework22.dao.sjdbc.SjdbcDishDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DaoConfig {

    @Bean
    public DishDao dishDao(DataSource dataSource) {

        return new SjdbcDishDao(dataSource);
    }

}
