package org.example.homework22.controller;

import org.example.homework22.dao.model.DishDao;
import org.example.homework22.model.Dish;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class DishController {

    private final DishDao dishDao;

    @Autowired
    public DishController(DishDao dishDao) {
        this.dishDao = dishDao;
    }

    @RequestMapping("/")
    public String home() {
        return
                "/list?name=[name] - search dish by name" + "<br />" +
                        "/add?name=[name]&ingredient=[ing]&mass=[mass]&ingredient=[ing]&mass=[mass]... - add dish by name and ingredients" + "<br />" +
                        "/remove?name=[name] - remove dish by name";
    }

    @RequestMapping(value = "/list")
    public List<Dish> list(
            @RequestParam(value = "name", required = false) String name
    ) {
        if (name == null) return dishDao.getAll();

        return dishDao.findByName(name);
    }

    @RequestMapping(value = "/add")
    public Dish add(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "ingredient") String[] ingredientNames,
            @RequestParam(value = "mass") Double[] ingredientMass
    ) {

        Dish dish = new Dish();
        dish.setName(name);

        Map<String, Double> ingredients = new HashMap<>();
        for (int i = 0; i < ingredientNames.length; i++) {
            ingredients.put(ingredientNames[i], ingredientMass[i]);
        }
        dish.setIngredients(ingredients);
        dishDao.create(dish);
        return dish;
    }

    @RequestMapping("/remove")
    public void remove(
            @RequestParam(value = "name") String name
    ) {

        dishDao.deleteByName(name);

    }


}
