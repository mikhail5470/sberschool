package org.example.homework22.model;

import java.util.HashMap;
import java.util.Map;

public class Dish {

    private Long id;
    private String name;
    private Map<String, Double> ingredients;

    public Dish() {
    }

    public Dish(Long id, String name, Map<String, Double> ingredients) {
        this.id = id;
        this.name = name;
        this.ingredients = new HashMap<>(ingredients);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Double> getIngredients() {
        return new HashMap<>(ingredients);
    }

    public void setIngredients(Map<String, Double> ingredients) {
        this.ingredients = new HashMap<>(ingredients);
    }

}
