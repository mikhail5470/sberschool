package org.example.homework22.dao.model;

import org.example.homework22.dao.Dao;
import org.example.homework22.model.Dish;

import java.util.List;

public interface DishDao extends Dao<Dish, Long> {

    Dish getByName(String name);

    List<Dish> findByName(String name);

    void deleteByName(String name);

}
