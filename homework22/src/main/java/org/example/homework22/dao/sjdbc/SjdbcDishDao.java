package org.example.homework22.dao.sjdbc;

import org.example.homework22.dao.model.DishDao;
import org.example.homework22.model.Dish;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SjdbcDishDao implements DishDao {

    private static final String SQL_GET_ALL =
            "SELECT dishes.id AS dish_id, dishes.name AS dish_name, " +
                    "ingredients.id AS ing_id, ingredients.name AS ing_name, ingredients.mass AS ing_mass " +
                    "FROM dishes " +
                    "LEFT JOIN ingredients " +
                    "ON ingredients.dish_id = dishes.id";
    private static final String SQL_INSERT =
            "INSERT INTO dishes (name) VALUES (?)";
    private static final String SQL_INSERT_INGREDIENTS =
            "INSERT INTO ingredients (dish_id, name, mass) VALUES (?, ?, ?)";
    private static final String SQL_DELETE_ALL =
            "DELETE FROM dishes";
    private static final String SQL_DELETE_ALL_INGREDIENTS =
            "DELETE FROM ingredients";
    private static final ResultSetExtractor<List<Dish>> DISH_RESULT_SET_EXTRACTOR = new DishResultSetExtractor();
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public SjdbcDishDao(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Dish get(Long key) {

        List<Dish> dishes = jdbcTemplate.query(SQL_GET_ALL + " WHERE dishes.id=?", new Object[]{key}, DISH_RESULT_SET_EXTRACTOR);
        if (dishes.size() == 0) return null;

        return dishes.get(0);
    }

    @Override
    public Long getCount() {
        
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM dishes", Long.class);
    }

    @Override
    public List<Dish> getAll() {

        return jdbcTemplate.query(SQL_GET_ALL, DISH_RESULT_SET_EXTRACTOR);
    }

    public Dish getByName(String name) {

        List<Dish> dishes = jdbcTemplate.query(SQL_GET_ALL + " WHERE dishes.name=?", new Object[]{name}, DISH_RESULT_SET_EXTRACTOR);
        if (dishes.size() == 0) return null;

        return dishes.get(0);
    }

    @Override
    public List<Dish> findByName(String name) {

        return jdbcTemplate.query(SQL_GET_ALL + " WHERE dishes.name LIKE ?", new Object[]{"%" + name + "%"}, DISH_RESULT_SET_EXTRACTOR);
    }

    @Override
    public void update(Dish dataObject) {
        if (dataObject.getId() == null) throw new IllegalArgumentException();

        throw new UnsupportedOperationException();
    }

    @Override
    public void create(Dish dataObject) {
        if (dataObject.getId() != null) throw new IllegalArgumentException();
        if (getByName(dataObject.getName()) != null)
            throw new IllegalArgumentException("this name already exists");

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(SQL_INSERT, new String[]{"id"});
                    ps.setString(1, dataObject.getName());
                    return ps;
                },
                keyHolder);
        dataObject.setId(keyHolder.getKey().longValue());

        List<Object[]> parameters = new ArrayList<>();
        for (Map.Entry<String, Double> entry : dataObject.getIngredients().entrySet()) {
            parameters.add(new Object[]{dataObject.getId(), entry.getKey(), entry.getValue()});
        }
        jdbcTemplate.batchUpdate(SQL_INSERT_INGREDIENTS, parameters);

    }

    private void deleteIngredients(Long dishID) {

        jdbcTemplate.update(SQL_DELETE_ALL_INGREDIENTS + " WHERE dish_id=?", new Object[]{dishID});
    }

    @Override
    public void delete(Dish dataObject) {
        if (dataObject.getId() == null) throw new IllegalArgumentException();

        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteByName(String name) {

        Dish dish = getByName(name);
        if (dish == null) throw new IllegalArgumentException("dish doesn't exist");

        deleteIngredients(dish.getId());
        jdbcTemplate.update(SQL_DELETE_ALL + " WHERE id=?", new Object[]{dish.getId()});

    }

    @Override
    public void deleteAll() {

        throw new UnsupportedOperationException();
    }

    private static class DishResultSetExtractor implements ResultSetExtractor<List<Dish>> {

        @Override
        public List<Dish> extractData(ResultSet resultSet) throws SQLException, DataAccessException {

            Map<Long, Dish> dishes = new HashMap<>();
            while (resultSet.next()) {
                Long dishID = resultSet.getLong("dish_id");
                if (!dishes.containsKey(dishID)) {
                    Dish dish = new Dish();
                    dish.setId(dishID);
                    dish.setName(resultSet.getString("dish_name"));
                    dish.setIngredients(new HashMap<>());
                    dishes.put(dishID, dish);
                }

                if (resultSet.getLong("ing_id") > 0) {
                    Dish dish = dishes.get(dishID);
                    Map<String, Double> ingredients = dish.getIngredients();
                    ingredients.put(resultSet.getString("ing_name"), resultSet.getDouble("ing_mass"));
                    dish.setIngredients(ingredients);
                }
            }

            return new ArrayList<>(dishes.values());
        }
    }
}
