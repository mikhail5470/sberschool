package org.example.homework21;

import org.example.homework21.manager.AccountManager;
import org.example.homework21.manager.CardManager;
import org.example.homework21.manager.ClientManager;
import org.example.homework21.model.Account;
import org.example.homework21.model.Card;
import org.example.homework21.model.Client;
import org.example.homework21.model.PersonName;
import org.example.homework21.terminal.Terminal;
import org.example.homework21.terminal.TerminalController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class MainConfig {

    @Bean
    public Terminal terminal() {

        Client client = new Client(new PersonName("John", "test", "test"));
        ClientManager clientManager = new ClientManager();
        clientManager.add(client);

        Account acc1 = new Account(client, 5_000_000d);
        Account acc2 = new Account(client, 0d);
        AccountManager accountManager = new AccountManager();
        accountManager.add(acc1);
        accountManager.add(acc2);

        CardManager cardManager = new CardManager();
        cardManager.add(new Card(acc1, "1234"));
        cardManager.add(new Card(acc2, "0000"));

        return new Terminal(clientManager, accountManager, cardManager);
    }

    @Bean
    public TerminalController terminalController(Terminal terminal) {
        return new TerminalController(terminal);
    }

}
