package org.example.homework21.model;

public class PersonName {

    private final String firstName;
    private final String secondName;
    private final String thirdName;

    public PersonName(String firstName, String secondName, String thirdName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.thirdName = thirdName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getThirdName() {
        return thirdName;
    }

    public String getFullName() {
        return new StringBuilder()
                .append(firstName).append(" ")
                .append(secondName).append(" ")
                .append(thirdName)
                .toString();
    }

    @Override
    public String toString() {
        return getFullName();
    }

}
