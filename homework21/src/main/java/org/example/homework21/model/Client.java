package org.example.homework21.model;

import java.util.UUID;

public class Client {

    private final String id;
    private final PersonName name;

    public Client(PersonName name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public PersonName getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id='" + id + '\'' +
                ", name=" + name +
                '}';
    }
}
