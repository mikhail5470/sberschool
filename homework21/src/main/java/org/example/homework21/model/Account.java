package org.example.homework21.model;

import org.example.homework21.terminal.exception.NotEnoughMoneyException;

import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {

    private final String id;
    private final Client client;
    private final Lock lock;
    private volatile Double balance;

    public Account(Client client, Double balance) {
        if (balance < 0) throw new IllegalArgumentException("cant be negative");
        this.id = UUID.randomUUID().toString();
        this.client = client;
        this.balance = balance;
        this.lock = new ReentrantLock();
    }

    public String getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public Double getBalance() {
        lock.lock();
        try {
            return balance;
        } finally {
            lock.unlock();
        }
    }

    public void put(Double cash) {
        if (cash <= 0) throw new IllegalArgumentException("must be positive");

        lock.lock();
        try {
            this.balance += cash;
        } finally {
            lock.unlock();
        }
    }

    public void take(Double cash) throws NotEnoughMoneyException {
        if (cash <= 0) throw new IllegalArgumentException("must be positive");
        if (cash > balance) throw new NotEnoughMoneyException();

        lock.lock();
        try {
            this.balance -= cash;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", client=" + client +
                ", balance=" + balance +
                '}';
    }
}
