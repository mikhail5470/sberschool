package org.example.homework21.model;

import java.util.Date;
import java.util.UUID;

public class Card {

    private final String id;
    private final Account account;
    private final String pin;

    private int attempts;
    private Date unlockDate;

    public Card(Account account, String pin) {
        this.id = UUID.randomUUID().toString();
        this.account = account;
        this.pin = pin;
        this.attempts = 0;
        this.unlockDate = new Date(0);
    }

    public String getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public String getPin() {
        return pin;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public Date getUnlockDate() {
        return unlockDate;
    }

    public void setUnlockDate(Date unlockDate) {
        this.unlockDate = unlockDate;
    }

    public boolean isLocked() {
        return unlockDate.after(new Date());
    }

    @Override
    public String toString() {
        return "Card{" +
                "id='" + id + '\'' +
                ", account=" + account +
                ", pin='" + pin + '\'' +
                ", attempts=" + attempts +
                ", unlockDate=" + unlockDate +
                '}';
    }
}
