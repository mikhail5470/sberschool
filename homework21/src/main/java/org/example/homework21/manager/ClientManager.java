package org.example.homework21.manager;

import org.example.homework21.model.Client;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ClientManager {

    private final Set<Client> clients;

    public ClientManager() {
        this.clients = ConcurrentHashMap.newKeySet();
    }

    public Set<Client> get() {
        return new HashSet<>(clients);
    }

    public void add(Client client) {
        if (!clients.add(client)) throw new IllegalArgumentException("already added");
    }

}
