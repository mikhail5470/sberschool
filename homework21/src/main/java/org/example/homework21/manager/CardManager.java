package org.example.homework21.manager;

import org.example.homework21.model.Account;
import org.example.homework21.model.Card;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class CardManager {

    private final Set<Card> cards;

    public CardManager() {
        this.cards = ConcurrentHashMap.newKeySet();
    }

    public Set<Card> get() {
        return new HashSet<>(cards);
    }

    public Card get(String id) {
        for (Card card : cards) {
            if (card.getId().equals(id)) return card;
        }
        return null;
    }

    public Set<Card> get(Account account) {
        Set<Card> accountCards = new HashSet<>();
        for (Card card : cards) {
            if (card.getAccount() == account) accountCards.add(card);
        }
        return accountCards;
    }

    public void add(Card card) {
        if (!cards.add(card)) throw new IllegalArgumentException("already added");
    }

}
