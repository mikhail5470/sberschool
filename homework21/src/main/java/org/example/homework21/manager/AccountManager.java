package org.example.homework21.manager;

import org.example.homework21.model.Account;
import org.example.homework21.model.Client;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class AccountManager {

    private final Set<Account> accounts;

    public AccountManager() {
        this.accounts = ConcurrentHashMap.newKeySet();
    }

    public Set<Account> get() {
        return new HashSet<>(accounts);
    }

    public Set<Account> get(Client client) {
        Set<Account> clientAccounts = new HashSet<>();
        for (Account account : accounts) {
            if (account.getClient() == client) clientAccounts.add(account);
        }

        return clientAccounts;
    }

    public void add(Account account) {
        if (!accounts.add(account)) throw new IllegalArgumentException("already added");
    }

}
