package org.example.homework21.terminal;

import org.example.homework21.model.Card;

public class TerminalSession {

    private final Card card;

    public TerminalSession(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }
}
