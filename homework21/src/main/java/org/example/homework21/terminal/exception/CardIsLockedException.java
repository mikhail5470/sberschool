package org.example.homework21.terminal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Date;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "card is locked")
public class CardIsLockedException extends Exception {

    public CardIsLockedException() {
        super("try later");
    }

    public CardIsLockedException(Date date) {
        super("wait to " + date.toString());
    }

    public CardIsLockedException(String message) {
        super(message);
    }

    public CardIsLockedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CardIsLockedException(Throwable cause) {
        super(cause);
    }
}
