package org.example.homework21.terminal;


import org.example.homework21.manager.AccountManager;
import org.example.homework21.manager.CardManager;
import org.example.homework21.manager.ClientManager;
import org.example.homework21.model.Card;
import org.example.homework21.terminal.exception.*;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Date;

@Service
public class Terminal {

    private static final int LOCK_CARD_ATTEMPTS = 3;
    private static final Duration LOCK_CARD_DURATION = Duration.ofSeconds(5L);

    private final ClientManager clientManager;
    private final AccountManager accountManager;
    private final CardManager cardManager;

    public Terminal(ClientManager clientManager, AccountManager accountManager, CardManager cardManager) {
        this.clientManager = clientManager;
        this.accountManager = accountManager;
        this.cardManager = cardManager;
    }

    public ClientManager getClientManager() {
        return clientManager;
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }

    public CardManager getCardManager() {
        return cardManager;
    }

    public TerminalSession authorize(String cardID, String pin) throws InvalidCardException, InvalidPinException, CardIsLockedException {
        final Card card = cardManager.get(cardID);
        if (card == null) throw new InvalidCardException();
        if (card.isLocked()) throw new CardIsLockedException(card.getUnlockDate());

        synchronized (card) {
            if (!card.getPin().equals(pin)) {
                card.setAttempts(card.getAttempts() + 1);
                if (card.getAttempts() >= LOCK_CARD_ATTEMPTS) {
                    card.setUnlockDate(new Date(System.currentTimeMillis() + LOCK_CARD_DURATION.toMillis()));
                    card.setAttempts(0);
                }
                throw new InvalidPinException();
            }

            card.setAttempts(0);
        }

        return new TerminalSession(card);
    }

    public Double getBalance(TerminalSession session) {
        return session.getCard().getAccount().getBalance();
    }

    public void addCash(TerminalSession session, Double cash) throws InvalidCashException {
        if ((cash < 1) || (cash % 100D != 0)) throw new InvalidCashException();

        session.getCard().getAccount().put(cash);
    }

    public void takeCash(TerminalSession session, Double cash) throws NotEnoughMoneyException, InvalidCashException {
        if ((cash < 1) || (cash % 100D != 0)) throw new InvalidCashException();

        session.getCard().getAccount().take(cash);
    }
}
