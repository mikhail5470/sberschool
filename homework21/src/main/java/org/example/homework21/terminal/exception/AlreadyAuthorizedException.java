package org.example.homework21.terminal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED, reason = "already authorized")
public class AlreadyAuthorizedException extends Exception {

    public AlreadyAuthorizedException() {
        super("don't try authorize more");
    }

    public AlreadyAuthorizedException(String message) {
        super(message);
    }

    public AlreadyAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlreadyAuthorizedException(Throwable cause) {
        super(cause);
    }
}
