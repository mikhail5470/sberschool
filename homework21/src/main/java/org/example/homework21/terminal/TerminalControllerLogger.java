package org.example.homework21.terminal;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
public class TerminalControllerLogger {

    private static final String PREFIX = "[LOG] ";

    private void output(String message) {
        System.out.println(PREFIX + message);
    }

    @Pointcut("within (org.example.homework21.terminal.TerminalController*) && @annotation(org.springframework.web.bind.annotation.RequestMapping)")
    private void loggingTarget() {
    }

    @Before("loggingTarget()")
    public void logCall(JoinPoint joinPoint) {
        output(String.format("CALL: %s: %s", joinPoint.getSignature(), Arrays.toString(joinPoint.getArgs())));
    }

    @AfterReturning(pointcut = "loggingTarget()", returning = "result")
    public void logReturn(JoinPoint joinPoint, Object result) {
        output(String.format("RETURN: %s: %s; RESULT: %s", joinPoint.getSignature(), Arrays.toString(joinPoint.getArgs()), result));
    }

    @AfterThrowing(pointcut = "loggingTarget()", throwing = "exception")
    public void logException(JoinPoint joinPoint, Exception exception) {
        output(String.format("ERROR: %s: %s; EXCEPTION: %s", joinPoint.getSignature(), Arrays.toString(joinPoint.getArgs()), exception));
    }
}
