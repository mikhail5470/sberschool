package org.example.homework21.terminal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "card doesn't exist")
public class InvalidCardException extends Exception {

    public InvalidCardException() {
        super("try other");
    }

    public InvalidCardException(String message) {
        super(message);
    }

    public InvalidCardException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCardException(Throwable cause) {
        super(cause);
    }
}
