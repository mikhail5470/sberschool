package org.example.homework21.terminal;

import org.example.homework21.model.Card;
import org.example.homework21.terminal.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Set;

@RestController
public class TerminalController {

    private static final String SESSION_ATTRIBUTE = "session";

    private final Terminal terminal;

    @Autowired
    public TerminalController(Terminal terminal) {
        this.terminal = terminal;
    }

    @RequestMapping(value = "/")
    public String info() {
        return "/cards - available cards" + "<br />" +
                "/authorize?id=[id]&pin=[pin] - authorize" + "<br />" +
                "/balance - balance" + "<br />" +
                "/put?value=[value] - put cash" + "<br />" +
                "/take?value=[value] - take cash" + "<br />" +
                "/exit - exit";
    }

    @RequestMapping(value = "/cards")
    public Set<Card> cards() {
        return terminal.getCardManager().get();
    }

    @RequestMapping(value = "/authorize")
    public Message authorize(
            @RequestParam(value = "id") String id,
            @RequestParam(value = "pin") String pin,
            HttpSession session
    ) throws AlreadyAuthorizedException, InvalidCardException, InvalidPinException, CardIsLockedException {
        if (session.getAttribute(SESSION_ATTRIBUTE) != null) throw new AlreadyAuthorizedException();

        session.setAttribute(SESSION_ATTRIBUTE, terminal.authorize(id, pin));
        return new Message("success");
    }

    @RequestMapping(value = "/balance")
    public Message balance(HttpSession session) throws NotAuthorizedException {
        if (session.getAttribute(SESSION_ATTRIBUTE) == null) throw new NotAuthorizedException();

        TerminalSession terminalSession = (TerminalSession) session.getAttribute("session");
        return new Message(terminal.getBalance(terminalSession));
    }

    @RequestMapping(value = "/put")
    public Message put(
            @RequestParam(value = "value") Double cash,
            HttpSession session
    ) throws NotAuthorizedException, InvalidCashException {
        if (session.getAttribute(SESSION_ATTRIBUTE) == null) throw new NotAuthorizedException();

        TerminalSession terminalSession = (TerminalSession) session.getAttribute("session");
        terminal.addCash(terminalSession, cash);
        return new Message("success");
    }

    @RequestMapping(value = "/take")
    public Message take(
            @RequestParam(value = "value") Double cash,
            HttpSession session
    ) throws NotAuthorizedException, InvalidCashException, NotEnoughMoneyException {
        if (session.getAttribute(SESSION_ATTRIBUTE) == null) throw new NotAuthorizedException();

        TerminalSession terminalSession = (TerminalSession) session.getAttribute("session");
        terminal.takeCash(terminalSession, cash);
        return new Message("success");
    }

    @RequestMapping(value = "/exit")
    public Message exit(HttpSession session) throws NotAuthorizedException {
        if (session.getAttribute(SESSION_ATTRIBUTE) == null) throw new NotAuthorizedException();

        session.setAttribute(SESSION_ATTRIBUTE, null);
        return new Message("success");
    }

}
