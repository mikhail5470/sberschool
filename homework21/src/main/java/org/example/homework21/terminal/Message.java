package org.example.homework21.terminal;

public class Message<T> {

    private final T data;

    public Message() {
        this.data = null;
    }

    public Message(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }
}
