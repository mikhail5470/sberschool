package org.example.homework21.terminal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "invalid value")
public class InvalidCashException extends Exception {

    public InvalidCashException() {
        super("cash must be a multiple of 100 and be positive");
    }

    public InvalidCashException(String message) {
        super(message);
    }

    public InvalidCashException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCashException(Throwable cause) {
        super(cause);
    }
}
