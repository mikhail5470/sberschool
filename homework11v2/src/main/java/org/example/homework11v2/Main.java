package org.example.homework11v2;

import org.example.homework11v2.thread.impl.FixedThreadPool;
import org.example.homework11v2.thread.impl.ScalableThreadPool;

import java.util.Random;

public class Main {

    public static Runnable getRandomTask() {
        return (() -> {
            //System.out.println(String.format("[starting] thread: %s", Thread.currentThread()));
            try {
                Thread.sleep(5000 + new Random().nextInt(5000));
            } catch (InterruptedException ignore) {
            }
            System.out.println(String.format("[finished] thread: %s; result: %s", Thread.currentThread(), new Random().nextInt(100)));
        });
    }

    public static void main(String[] args) throws InterruptedException {

        //FixedThreadPool pool = new FixedThreadPool( 10);
        ScalableThreadPool pool = new ScalableThreadPool(3, 7);

        pool.start();

        for (int i = 0; i < 20; i++) {
            Thread.sleep(500);
            pool.execute(getRandomTask());
        }


    }
}
