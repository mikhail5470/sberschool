package org.example.homework11v2.thread.impl;

import org.example.homework11v2.thread.ThreadPool;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {

    private class Executor implements Runnable {
        @Override
        public void run() {
            while (!tryDownScale()) {
                Runnable task;
                synchronized (queueLock) {
                    try {
                        task = tasks.poll();
                        while (task == null) {
                            queueLock.wait();
                            task = tasks.poll();
                        }
                    } catch (InterruptedException ignore) {
                        return;
                    } finally {
                        queueLock.notifyAll();
                    }
                }
                task.run();
            }
        }
    }

    private final Object queueLock = new Object();

    private boolean started;
    private final Thread[] coreThreads;
    private final Thread[] tempThreads;
    private final Queue<Runnable> tasks;
    private int tempThreadsCount;

    public ScalableThreadPool(int min, int max) {
        if (min < 1) throw new IllegalArgumentException("min must be positive");
        if (min > max) throw new IllegalArgumentException("min must be less or equal max");

        this.tasks = new ArrayDeque<>();

        this.started = false;
        this.tempThreadsCount = 0;
        this.tempThreads = new Thread[max - min];
        this.coreThreads = new Thread[min];
        for (int i = 0; i < min; i++) {
            this.coreThreads[i] = buildThread();
        }
    }

    private Thread buildThread() {
        return new Thread(new Executor());
    }

    private int getScalingFactorTarget() {
        int needed = tasks.size();
        int free = tempThreads.length - tempThreadsCount;
        return (needed == 0) ? (-tempThreadsCount) : Math.min(needed, free);
    }

    synchronized private boolean tryDownScale() {
        if (getScalingFactorTarget() > -1) return false;

        tempThreadsCount--;
        System.out.println(String.format("[scaleDOWN] temp threads: %d; tasks: %d", tempThreadsCount, tasks.size()));

        return true;
    }

    synchronized private boolean tryUpScale() {
        int target = getScalingFactorTarget();
        if (target < 1) return false;

        for (int i = 0; i < target; i++) {
            Thread thread = buildThread();
            tempThreads[tempThreadsCount] = thread;
            tempThreadsCount++;

            if (started) thread.start();
        }
        System.out.println(String.format("[scaleUP] temp threads: %d; tasks: %d", tempThreadsCount, tasks.size()));

        return true;
    }

    @Override
    public void start() {
        started = true;
        for (Thread thread : coreThreads) {
            thread.start();
        }
        for (int i = 0; i < tempThreadsCount; i++) {
            tempThreads[i].start();
        }
    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (queueLock) {
            try {
                while (!tasks.offer(runnable)) {
                    queueLock.wait();
                }
                tryUpScale();
            } catch (InterruptedException ignore) {
                return;
            } finally {
                queueLock.notifyAll();
            }
        }
    }
}
