package org.example.homework11v2.thread;

public interface ThreadPool {

    void start();

    void execute(Runnable runnable);

}
