package org.example.homework11v2.thread.impl;

import org.example.homework11v2.thread.ThreadPool;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {

    private class Executor implements Runnable {
        @Override
        public void run() {
            while (true) {
                Runnable task;
                synchronized (queueLock) {
                    try {
                        task = tasks.poll();
                        while (task == null) {
                            queueLock.wait();
                            task = tasks.poll();
                        }
                    } catch (InterruptedException ignore) {
                        return;
                    } finally {
                        queueLock.notifyAll();
                    }
                }
                task.run();
            }
        }
    }

    private final Object queueLock = new Object();

    private final Thread[] threads;
    private final Queue<Runnable> tasks;

    public FixedThreadPool(int size) {
        if (size < 1) throw new IllegalArgumentException("must be positive");

        this.tasks = new ArrayDeque<>();
        this.threads = new Thread[size];
        for (int i = 0; i < size; i++) {
            threads[i] = new Thread(new Executor());
        }
    }

    @Override
    public void start() {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (queueLock) {
            try {
                while (!tasks.offer(runnable)) {
                    queueLock.wait();
                }
            } catch (InterruptedException ignore) {
                return;
            } finally {
                queueLock.notifyAll();
            }
        }
    }
}
