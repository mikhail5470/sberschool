package org.example.homework17.example3.transport.impl;

import org.example.homework17.example3.Route;
import org.example.homework17.example3.transport.Transport;

public class Truck implements Transport {

    private Route route;

    @Override
    public void go() {
        // some implementation
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
