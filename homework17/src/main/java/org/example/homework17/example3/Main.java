package org.example.homework17.example3;

import org.example.homework17.example3.transport.impl.Truck;

public class Main {

    public static void main(String[] args) {
        // Нужно применить шаблон, благодаря которому можно было бы использовать этот же код с другим транспоритным средством
        // например Ship или Aircraft и при добавлении нового класса транспортного средства этот код не нужно было бы изменять

        LogisticSystem logisticSystem = new LogisticSystem();

        Point departure = new Point("Moscow");
        Point destination = new Point("Saint-Petersburg");

        Route route = logisticSystem.createRoute(LogisticSystem.Transport.TRUCK, departure, destination);
        Truck truck = new Truck();
        truck.setRoute(route);
        truck.go();
    }
}

