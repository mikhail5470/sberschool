package org.example.homework17.example3;

import java.util.ArrayList;

public class LogisticSystem {

    public Route createRoute(Transport transport, Point departure, Point destination) {
        switch (transport) {
            case SHIP: {
                // logic
                return new Route(new ArrayList<>());
            }
            case TRUCK: {
                // logic
                return new Route(new ArrayList<>());
            }
            case AIRCRAFT: {
                // logic
                return new Route(new ArrayList<>());
            }
        }
        return new Route(new ArrayList<>());
    }

    public enum Transport {
        TRUCK, SHIP, AIRCRAFT
    }
}
