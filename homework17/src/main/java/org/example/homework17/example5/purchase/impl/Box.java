package org.example.homework17.example5.purchase.impl;

import org.example.homework17.example5.purchase.Purchase;

import java.util.ArrayList;
import java.util.List;

public class Box implements Purchase {

    private List<Purchase> purchases = new ArrayList<>();

    public void addPurchase(Purchase purchase) {
        purchases.add(purchase);
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    @Override
    public double getPrice() {
        double price = 0d;
        for (Purchase purchase : purchases) {
            price += purchase.getPrice();
        }
        return price;
    }
}
