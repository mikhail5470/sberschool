package org.example.homework17.example5.purchase;

public interface Purchase {
    double getPrice();
}
