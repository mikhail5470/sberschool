package org.example.homework17.example5.purchase.impl;

import org.example.homework17.example5.purchase.Purchase;

public class Product implements Purchase {

    private String name;
    private double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
