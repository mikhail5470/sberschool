package org.example.homework17.example5;

import org.example.homework17.example5.purchase.Purchase;

import java.util.List;

public class Cashier {

    public double calculatePrice(List<Purchase> purchases) {
        double price = 0d;
        for (Purchase purchase : purchases) {
            price += purchase.getPrice();
        }
        return price;
    }
}
