package org.example.homework17.example4;

public class Main {

    public static void main(String[] args) {
        someMethod1();
        someMethod2();
        someMethod3();
    }

    public static void someMethod1() {
        // что-то делает с database
        Database.getInstance().getData(1);
    }

    public static void someMethod2() {
        // что-то делает с database
        Database.getInstance().getData(1);
    }

    public static void someMethod3() {
        // что-то делает с database
        Database.getInstance().getData(1);
    }
}
