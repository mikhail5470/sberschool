package org.example.homework17.example4;

public class Database {

    private static final String ADDRESS = "some";

    private static volatile Database instance;

    private Database() {
        // some
    }

    public static Database getInstance() {
        if (instance == null) {
            synchronized (Database.class) {
                if (instance == null) instance = new Database();
            }
        }
        return instance;
    }

    public String getData(long id) {
        // implementation
        return "some";
    }

    public void saveData(String data, long id) {
        // implementation
    }
}
