package org.example.homework17.example1.factory.impl;

import org.example.homework17.example1.chassis.Chassis;
import org.example.homework17.example1.chassis.impl.VanChassis;
import org.example.homework17.example1.engine.Engine;
import org.example.homework17.example1.engine.impl.VanEngine;
import org.example.homework17.example1.factory.VehicleFactory;
import org.example.homework17.example1.wheels.Wheels;
import org.example.homework17.example1.wheels.impl.VanWheels;

public class VanFactory implements VehicleFactory {

    @Override
    public Engine createEngine() {
        return new VanEngine();
    }

    @Override
    public Chassis createChassis() {
        return new VanChassis();
    }

    @Override
    public Wheels createWheels() {
        return new VanWheels();
    }
}
