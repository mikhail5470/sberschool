package org.example.homework17.example1.vehicle;

import org.example.homework17.example1.chassis.Chassis;
import org.example.homework17.example1.engine.Engine;
import org.example.homework17.example1.wheels.Wheels;

public abstract class AbstractVehicle implements Vehicle {

    private Chassis chassis;
    private Engine engine;
    private Wheels wheels;

    public AbstractVehicle(Chassis chassis, Engine engine, Wheels wheels) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
    }

    @Override
    public Chassis getChassis() {
        return chassis;
    }

    @Override
    public Engine getEngine() {
        return engine;
    }

    @Override
    public Wheels getWheels() {
        return wheels;
    }
}
