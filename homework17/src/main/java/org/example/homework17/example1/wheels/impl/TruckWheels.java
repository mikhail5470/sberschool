package org.example.homework17.example1.wheels.impl;

import org.example.homework17.example1.wheels.Wheels;

public class TruckWheels implements Wheels {

    private String parts;

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
