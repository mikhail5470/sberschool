package org.example.homework17.example1.chassis.impl;

import org.example.homework17.example1.chassis.Chassis;

public class CarChassis implements Chassis {

    private String parts;

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "org.example.homework17.example2.parts.chassis.impl.CarChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
