package org.example.homework17.example1.factory.impl;

import org.example.homework17.example1.chassis.Chassis;
import org.example.homework17.example1.chassis.impl.TruckChassis;
import org.example.homework17.example1.engine.Engine;
import org.example.homework17.example1.engine.impl.TruckEngine;
import org.example.homework17.example1.factory.VehicleFactory;
import org.example.homework17.example1.wheels.Wheels;
import org.example.homework17.example1.wheels.impl.TruckWheels;

public class TruckFactory implements VehicleFactory {
    @Override
    public Engine createEngine() {
        return new TruckEngine();
    }

    @Override
    public Chassis createChassis() {
        return new TruckChassis();
    }

    @Override
    public Wheels createWheels() {
        return new TruckWheels();
    }
}
