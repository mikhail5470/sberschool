package org.example.homework17.example1.factory;

import org.example.homework17.example1.chassis.Chassis;
import org.example.homework17.example1.engine.Engine;
import org.example.homework17.example1.wheels.Wheels;

public interface VehicleFactory {

    Engine createEngine();

    Chassis createChassis();

    Wheels createWheels();

}
