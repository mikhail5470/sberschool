package org.example.homework17.example1.factory.impl;

import org.example.homework17.example1.chassis.Chassis;
import org.example.homework17.example1.chassis.impl.CarChassis;
import org.example.homework17.example1.engine.Engine;
import org.example.homework17.example1.engine.impl.CarEngine;
import org.example.homework17.example1.factory.VehicleFactory;
import org.example.homework17.example1.wheels.Wheels;
import org.example.homework17.example1.wheels.impl.CarWheels;

public class CarFactory implements VehicleFactory {

    @Override
    public Engine createEngine() {
        return new CarEngine();
    }

    @Override
    public Chassis createChassis() {
        return new CarChassis();
    }

    @Override
    public Wheels createWheels() {
        return new CarWheels();
    }
}
