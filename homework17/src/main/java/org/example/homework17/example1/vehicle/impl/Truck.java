package org.example.homework17.example1.vehicle.impl;

import org.example.homework17.example1.chassis.Chassis;
import org.example.homework17.example1.engine.Engine;
import org.example.homework17.example1.vehicle.AbstractVehicle;
import org.example.homework17.example1.wheels.Wheels;

public class Truck extends AbstractVehicle {

    public Truck(Chassis chassis, Engine carEngine, Wheels carWheels) {
        super(chassis, carEngine, carWheels);
    }

    @Override
    public String toString() {
        return "Truck{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                '}';
    }
}
