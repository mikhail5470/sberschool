package org.example.homework17.example1;

import org.example.homework17.example1.factory.VehicleFactory;
import org.example.homework17.example1.factory.impl.CarFactory;
import org.example.homework17.example1.factory.impl.TruckFactory;
import org.example.homework17.example1.factory.impl.VanFactory;
import org.example.homework17.example1.vehicle.impl.Car;
import org.example.homework17.example1.vehicle.impl.Truck;
import org.example.homework17.example1.vehicle.impl.Van;

public class Main {

    public static void main(String[] args) {
        VehicleFactory factory = new CarFactory();
        Car car = new Car(factory.createChassis(), factory.createEngine(), factory.createWheels());
        System.out.println(car);

        factory = new VanFactory();
        Van van = new Van(factory.createChassis(), factory.createEngine(), factory.createWheels());

        factory = new TruckFactory();
        Truck truck = new Truck(factory.createChassis(), factory.createEngine(), factory.createWheels());
    }


}
