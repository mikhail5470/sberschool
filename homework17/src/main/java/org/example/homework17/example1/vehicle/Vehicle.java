package org.example.homework17.example1.vehicle;

import org.example.homework17.example1.chassis.Chassis;
import org.example.homework17.example1.engine.Engine;
import org.example.homework17.example1.wheels.Wheels;

public interface Vehicle {
    Chassis getChassis();

    Engine getEngine();

    Wheels getWheels();
}
