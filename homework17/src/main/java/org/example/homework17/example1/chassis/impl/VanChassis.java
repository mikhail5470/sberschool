package org.example.homework17.example1.chassis.impl;

import org.example.homework17.example1.chassis.Chassis;

public class VanChassis implements Chassis {

    private String parts;

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
