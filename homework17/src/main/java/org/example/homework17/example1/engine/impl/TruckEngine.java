package org.example.homework17.example1.engine.impl;

import org.example.homework17.example1.engine.Engine;

public class TruckEngine implements Engine {

    private String parts;

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
