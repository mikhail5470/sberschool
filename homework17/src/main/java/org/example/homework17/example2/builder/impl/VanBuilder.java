package org.example.homework17.example2.builder.impl;

import org.example.homework17.example2.builder.AbstractVehicleBuilder;
import org.example.homework17.example2.parts.chassis.impl.VanChassis;
import org.example.homework17.example2.parts.engine.impl.VanEngine;
import org.example.homework17.example2.parts.wheels.impl.VanWheels;
import org.example.homework17.example2.vehicle.Vehicle;
import org.example.homework17.example2.vehicle.impl.Van;

public class VanBuilder extends AbstractVehicleBuilder {

    @Override
    public void buildChassis() {
        chassis = new VanChassis();
    }

    @Override
    public void buildEngine() {
        engine = new VanEngine();
    }

    @Override
    public void buildWheels() {
        wheels = new VanWheels();
    }

    @Override
    public Vehicle getVehicle() {
        return new Van(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }

}
