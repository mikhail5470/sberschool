package org.example.homework17.example2.director;

import org.example.homework17.example2.builder.VehicleBuilder;
import org.example.homework17.example2.vehicle.Vehicle;

public interface VehicleDirector {

    Vehicle build(VehicleBuilder builder);

}
