package org.example.homework17.example2.builder.impl;

import org.example.homework17.example2.builder.AbstractVehicleBuilder;
import org.example.homework17.example2.parts.chassis.impl.CarChassis;
import org.example.homework17.example2.parts.engine.impl.CarEngine;
import org.example.homework17.example2.parts.wheels.impl.CarWheels;
import org.example.homework17.example2.vehicle.Vehicle;
import org.example.homework17.example2.vehicle.impl.Car;

public class CarBuilder extends AbstractVehicleBuilder {

    @Override
    public void buildChassis() {
        chassis = new CarChassis();
    }

    @Override
    public void buildEngine() {
        engine = new CarEngine();
    }

    @Override
    public void buildWheels() {
        wheels = new CarWheels();
    }

    @Override
    public Vehicle getVehicle() {
        return new Car(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }
}
