package org.example.homework17.example2.parts.engine.impl;

import org.example.homework17.example2.parts.engine.Engine;

public class TruckEngine implements Engine {

    private String parts;

    public TruckEngine() {
        this.parts = "parts of truck engine";
    }

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
