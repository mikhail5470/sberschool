package org.example.homework17.example2.parts.wheels.impl;

import org.example.homework17.example2.parts.wheels.Wheels;

public class TruckWheels implements Wheels {

    private String parts;

    public TruckWheels() {
        this.parts = "parts of truck wheels";
    }

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
