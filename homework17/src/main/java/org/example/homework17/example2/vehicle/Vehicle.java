package org.example.homework17.example2.vehicle;

import org.example.homework17.example2.parts.brakes.ABS;
import org.example.homework17.example2.parts.chassis.Chassis;
import org.example.homework17.example2.parts.climate.AirConditioning;
import org.example.homework17.example2.parts.climate.ClimateControlSystem;
import org.example.homework17.example2.parts.climate.SeatHeating;
import org.example.homework17.example2.parts.engine.Engine;
import org.example.homework17.example2.parts.steering.Steering;
import org.example.homework17.example2.parts.wheels.Wheels;

public interface Vehicle {

    Chassis getChassis();

    Engine getEngine();

    Wheels getWheels();

    Steering getSteering();

    ClimateControlSystem getClimateControlSystem();

    AirConditioning getAirConditioning();

    SeatHeating getSeatHeating();

    ABS getAbs();

}
