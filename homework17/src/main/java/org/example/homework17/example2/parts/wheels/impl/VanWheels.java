package org.example.homework17.example2.parts.wheels.impl;

import org.example.homework17.example2.parts.wheels.Wheels;

public class VanWheels implements Wheels {

    private String parts;

    public VanWheels() {
        this.parts = "parts of van wheels";
    }

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
