package org.example.homework17.example2.builder.impl;

import org.example.homework17.example2.builder.AbstractVehicleBuilder;
import org.example.homework17.example2.parts.chassis.impl.TruckChassis;
import org.example.homework17.example2.parts.engine.impl.TruckEngine;
import org.example.homework17.example2.parts.wheels.impl.TruckWheels;
import org.example.homework17.example2.vehicle.Vehicle;
import org.example.homework17.example2.vehicle.impl.Truck;

public class TruckBuilder extends AbstractVehicleBuilder {

    @Override
    public void buildChassis() {
        chassis = new TruckChassis();
    }

    @Override
    public void buildEngine() {
        engine = new TruckEngine();
    }

    @Override
    public void buildWheels() {
        wheels = new TruckWheels();
    }

    @Override
    public Vehicle getVehicle() {
        return new Truck(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }

}
