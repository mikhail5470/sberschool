package org.example.homework17.example2.parts.engine.impl;

import org.example.homework17.example2.parts.engine.Engine;

public class VanEngine implements Engine {

    private String parts;

    public VanEngine() {
        this.parts = "parts of van engine";
    }

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
