package org.example.homework17.example2.parts.climate;

public class ClimateControlSystem {

    private String parts;

    public ClimateControlSystem() {
        this.parts = "parts of climate control system";
    }

    public String getParts() {
        return parts;
    }
}
