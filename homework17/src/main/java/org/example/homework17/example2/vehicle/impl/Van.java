package org.example.homework17.example2.vehicle.impl;

import org.example.homework17.example2.parts.brakes.ABS;
import org.example.homework17.example2.parts.chassis.Chassis;
import org.example.homework17.example2.parts.climate.AirConditioning;
import org.example.homework17.example2.parts.climate.ClimateControlSystem;
import org.example.homework17.example2.parts.climate.SeatHeating;
import org.example.homework17.example2.parts.engine.Engine;
import org.example.homework17.example2.parts.steering.Steering;
import org.example.homework17.example2.parts.wheels.Wheels;
import org.example.homework17.example2.vehicle.AbstractVehicle;

public class Van extends AbstractVehicle {

    public Van(Chassis chassis, Engine engine, Wheels wheels, Steering steering, ClimateControlSystem climateControlSystem, AirConditioning airConditioning, SeatHeating seatHeating, ABS abs) {
        super(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }

    @Override
    public String toString() {
        return "Van{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                ", steering=" + getSteering() +
                ", climateControlSystem=" + getClimateControlSystem() +
                ", airConditioning=" + getAirConditioning() +
                ", seatHeating=" + getSeatHeating() +
                ", abs=" + getAbs() +
                '}';
    }
}
