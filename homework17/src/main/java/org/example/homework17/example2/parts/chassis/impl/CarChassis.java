package org.example.homework17.example2.parts.chassis.impl;

import org.example.homework17.example2.parts.chassis.Chassis;

public class CarChassis implements Chassis {

    private String parts;

    public CarChassis() {
        this.parts = "parts of car chassis";
    }

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "org.example.homework17.example2.parts.chassis.impl.CarChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
