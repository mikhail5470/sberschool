package org.example.homework17.example2.builder;

import org.example.homework17.example2.vehicle.Vehicle;

public interface VehicleBuilder {

    void buildChassis();

    void buildEngine();

    void buildWheels();

    void buildStandardSteering();

    void buildPowerSteering();

    void buildAirConditioning();

    void buildClimateControlSystem();

    void buildSeatHeating();

    void buildABS();

    Vehicle getVehicle();

}
