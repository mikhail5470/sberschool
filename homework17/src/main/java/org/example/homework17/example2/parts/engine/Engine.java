package org.example.homework17.example2.parts.engine;

public interface Engine {

    String getEngineParts();
}
