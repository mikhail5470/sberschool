package org.example.homework17.example2.parts.chassis;

public interface Chassis {

    String getChassisParts();
}
