package org.example.homework17.example2.director.impl;

import org.example.homework17.example2.builder.VehicleBuilder;
import org.example.homework17.example2.director.VehicleDirector;
import org.example.homework17.example2.vehicle.Vehicle;

public class LuxDirector implements VehicleDirector {

    @Override
    public Vehicle build(VehicleBuilder builder) {
        builder.buildChassis();
        builder.buildEngine();
        builder.buildWheels();
        builder.buildPowerSteering();
        builder.buildAirConditioning();
        builder.buildClimateControlSystem();
        builder.buildSeatHeating();
        builder.buildABS();
        return builder.getVehicle();
    }
}
