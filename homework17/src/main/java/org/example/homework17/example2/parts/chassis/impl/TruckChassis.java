package org.example.homework17.example2.parts.chassis.impl;

import org.example.homework17.example2.parts.chassis.Chassis;

public class TruckChassis implements Chassis {

    private String parts;

    public TruckChassis() {
        this.parts = "parts of truck chassis";
    }

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
