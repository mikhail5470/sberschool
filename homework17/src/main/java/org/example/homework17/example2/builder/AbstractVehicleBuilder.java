package org.example.homework17.example2.builder;

import org.example.homework17.example2.parts.brakes.ABS;
import org.example.homework17.example2.parts.chassis.Chassis;
import org.example.homework17.example2.parts.climate.AirConditioning;
import org.example.homework17.example2.parts.climate.ClimateControlSystem;
import org.example.homework17.example2.parts.climate.SeatHeating;
import org.example.homework17.example2.parts.engine.Engine;
import org.example.homework17.example2.parts.steering.Steering;
import org.example.homework17.example2.parts.steering.impl.PowerSteering;
import org.example.homework17.example2.parts.steering.impl.StandardSteering;
import org.example.homework17.example2.parts.wheels.Wheels;

public abstract class AbstractVehicleBuilder implements VehicleBuilder {

    protected Chassis chassis;
    protected Engine engine;
    protected Wheels wheels;
    protected Steering steering;
    protected AirConditioning airConditioning;
    protected ClimateControlSystem climateControlSystem;
    protected SeatHeating seatHeating;
    protected ABS abs;

    @Override
    public void buildStandardSteering() {
        steering = new StandardSteering();
    }

    @Override
    public void buildPowerSteering() {
        steering = new PowerSteering();
    }

    @Override
    public void buildAirConditioning() {
        airConditioning = new AirConditioning();
    }

    @Override
    public void buildClimateControlSystem() {
        climateControlSystem = new ClimateControlSystem();
    }

    @Override
    public void buildSeatHeating() {
        seatHeating = new SeatHeating();
    }

    @Override
    public void buildABS() {
        abs = new ABS();
    }

}
