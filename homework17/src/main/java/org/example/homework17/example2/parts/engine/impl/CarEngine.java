package org.example.homework17.example2.parts.engine.impl;


import org.example.homework17.example2.parts.engine.Engine;

public class CarEngine implements Engine {

    private String parts;

    public CarEngine() {
        this.parts = "parts of car engine";
    }

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
