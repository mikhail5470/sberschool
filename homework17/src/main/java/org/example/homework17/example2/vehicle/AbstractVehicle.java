package org.example.homework17.example2.vehicle;

import org.example.homework17.example2.parts.brakes.ABS;
import org.example.homework17.example2.parts.chassis.Chassis;
import org.example.homework17.example2.parts.climate.AirConditioning;
import org.example.homework17.example2.parts.climate.ClimateControlSystem;
import org.example.homework17.example2.parts.climate.SeatHeating;
import org.example.homework17.example2.parts.engine.Engine;
import org.example.homework17.example2.parts.steering.Steering;
import org.example.homework17.example2.parts.wheels.Wheels;

public abstract class AbstractVehicle implements Vehicle {

    private Chassis chassis;
    private Engine engine;
    private Wheels wheels;
    private Steering steering;
    private ClimateControlSystem climateControlSystem;
    private AirConditioning airConditioning;
    private SeatHeating seatHeating;
    private ABS abs;

    public AbstractVehicle(Chassis chassis,
                           Engine engine,
                           Wheels wheels,
                           Steering steering,
                           ClimateControlSystem climateControlSystem,
                           AirConditioning airConditioning,
                           SeatHeating seatHeating,
                           ABS abs) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
        this.steering = steering;
        this.climateControlSystem = climateControlSystem;
        this.airConditioning = airConditioning;
        this.seatHeating = seatHeating;
        this.abs = abs;
    }

    public Chassis getChassis() {
        return chassis;
    }

    public Engine getEngine() {
        return engine;
    }

    public Wheels getWheels() {
        return wheels;
    }

    public Steering getSteering() {
        return steering;
    }

    public ClimateControlSystem getClimateControlSystem() {
        return climateControlSystem;
    }

    public AirConditioning getAirConditioning() {
        return airConditioning;
    }

    public SeatHeating getSeatHeating() {
        return seatHeating;
    }

    public ABS getAbs() {
        return abs;
    }
}
