package org.example.homework17.example2.parts.chassis.impl;

import org.example.homework17.example2.parts.chassis.Chassis;

public class VanChassis implements Chassis {

    private String parts;

    public VanChassis() {
        this.parts = "parts of van chassis";
    }

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
