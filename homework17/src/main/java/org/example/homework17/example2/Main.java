package org.example.homework17.example2;

import org.example.homework17.example2.builder.VehicleBuilder;
import org.example.homework17.example2.builder.impl.CarBuilder;
import org.example.homework17.example2.builder.impl.TruckBuilder;
import org.example.homework17.example2.builder.impl.VanBuilder;
import org.example.homework17.example2.director.VehicleDirector;
import org.example.homework17.example2.director.impl.LuxDirector;
import org.example.homework17.example2.director.impl.StandardDirector;
import org.example.homework17.example2.vehicle.Vehicle;

public class Main {
    public static void main(String[] args) {
        VehicleDirector standardDirector = new StandardDirector();
        VehicleDirector luxDirector = new LuxDirector();

        VehicleBuilder carBuilder = new CarBuilder();
        Vehicle standardCar = standardDirector.build(carBuilder);
        Vehicle luxCar = luxDirector.build(carBuilder);

        VehicleBuilder vanBuilder = new VanBuilder();
        Vehicle standardVan = standardDirector.build(vanBuilder);
        Vehicle luxVan = luxDirector.build(vanBuilder);

        VehicleBuilder truckBuilder = new TruckBuilder();
        Vehicle standardTruck = standardDirector.build(truckBuilder);
        Vehicle luxTruck = luxDirector.build(truckBuilder);

        System.out.println(standardCar);
        System.out.println(luxCar);

        System.out.println(standardVan);
        System.out.println(luxVan);

        System.out.println(standardTruck);
        System.out.println(luxTruck);

    }
}