package org.example.homework11v1;

public class Foo {

    synchronized public void first() {
        System.out.println("first");
    }

    synchronized public void second() {
        System.out.println("second");
    }

    synchronized public void third() {
        System.out.println("third");
    }

}
