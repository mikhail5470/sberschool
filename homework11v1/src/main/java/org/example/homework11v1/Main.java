package org.example.homework11v1;

import java.util.ArrayList;

public class Main {

    public static void foo() {
        Foo foo = new Foo();
        ArrayList<Runnable> tasks = new ArrayList<>();
        tasks.add(foo::first);
        tasks.add(foo::second);
        tasks.add(foo::third);

        SynchronizedWorker worker = new SynchronizedWorker(tasks);
        worker.work();

    }

    public static void main(String[] args) {
        foo();
    }
}
