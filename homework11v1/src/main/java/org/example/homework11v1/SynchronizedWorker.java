package org.example.homework11v1;

import java.util.ArrayList;

public class SynchronizedWorker {

    private final ArrayList<Runnable> tasks;

    public SynchronizedWorker(ArrayList<Runnable> tasks) {
        this.tasks = new ArrayList<>(tasks);
        //Collections.reverse(this.tasks);
    }

    private Thread getWaiter(Runnable runnable, Thread waitTo) {
        return new Thread(() -> {
            System.out.println("running: " + Thread.currentThread());
            try {
                System.out.println(Thread.currentThread() + " is waiting to: " + waitTo);
                waitTo.join();
            } catch (InterruptedException e) {
                // nothing
            }
            runnable.run();
        });
    }

    public void work() {
        Thread thread = new Thread();
        for (Runnable task : tasks) {
            thread = getWaiter(task, thread);
            System.out.println("starting: " + thread);
            thread.start();
        }
    }

}
