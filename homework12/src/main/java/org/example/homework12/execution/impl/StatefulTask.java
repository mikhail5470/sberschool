package org.example.homework12.execution.impl;

public class StatefulTask implements Runnable {

    public enum State {
        NEW,
        RUNNING,
        COMPLETED,
        INTERRUPTED,
        FAILED
    }

    private State state;
    private final Runnable task;
    private final TasksCallback tasksCallback;

    public StatefulTask(Runnable task, TasksCallback tasksCallback) {
        this.state = State.NEW;
        this.task = task;
        this.tasksCallback = tasksCallback;
    }

    @Override
    public void run() {
        if (state == State.INTERRUPTED) return;

        state = State.RUNNING;
        try {
            task.run();
            state = State.COMPLETED;
        } catch (Exception e) {
            state = State.FAILED;
            e.printStackTrace();
        }
        tasksCallback.finished();
    }

    public void interrupt() {
        if (state != State.NEW) return;

        state = State.INTERRUPTED;
        tasksCallback.finished();
    }

    public State getState() {
        return state;
    }
}
