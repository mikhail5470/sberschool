package org.example.homework12.execution;

public interface ExecutionManager {

    Context execute(Runnable callback, Runnable... tasks);

}
