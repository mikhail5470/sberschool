package org.example.homework12.execution.impl;

import org.example.homework12.execution.Context;

public class ContextImpl implements Context {

    private final StatefulTask[] tasks;

    public ContextImpl(StatefulTask[] tasks) {
        this.tasks = tasks;
    }

    @Override
    public int getCompletedTaskCount() {
        int count = 0;
        for (StatefulTask task : tasks) {
            if (task.getState() == StatefulTask.State.COMPLETED) count++;
        }
        return count;
    }

    @Override
    public int getFailedTaskCount() {
        int count = 0;
        for (StatefulTask task : tasks) {
            if (task.getState() == StatefulTask.State.FAILED) count++;
        }
        return count;
    }

    @Override
    public int getInterruptedTaskCount() {
        int count = 0;
        for (StatefulTask task : tasks) {
            if (task.getState() == StatefulTask.State.INTERRUPTED) count++;
        }
        return count;
    }

    @Override
    synchronized public void interrupt() {
        for (StatefulTask task : tasks) {
            task.interrupt();
        }
    }

    @Override
    public boolean isFinished() {
        for (StatefulTask task : tasks) {
            StatefulTask.State state = task.getState();
            if (state == StatefulTask.State.NEW || state == StatefulTask.State.RUNNING) return false;
        }
        return true;
    }
}
