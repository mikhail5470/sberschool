package org.example.homework12.execution.impl;

public class TasksCallback {

    private int countDown;
    private final Runnable callback;

    public TasksCallback(Runnable callback, int countDown) {
        this.countDown = countDown;
        this.callback = callback;
    }

    synchronized public void finished() {
        countDown--;
        if (countDown == 0) callback.run();
    }

}
