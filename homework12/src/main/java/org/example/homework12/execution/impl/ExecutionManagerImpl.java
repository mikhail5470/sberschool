package org.example.homework12.execution.impl;

import org.example.homework12.execution.Context;
import org.example.homework12.execution.ExecutionManager;
import org.example.homework12.threadpool.ThreadPool;

public class ExecutionManagerImpl implements ExecutionManager {

    private final ThreadPool threadPool;

    public ExecutionManagerImpl(ThreadPool threadPool) {
        this.threadPool = threadPool;
        this.threadPool.start();
    }


    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        TasksCallback tasksCallback = new TasksCallback(callback, tasks.length);
        StatefulTask[] statefulTasks = new StatefulTask[tasks.length];
        for (int i = 0; i < tasks.length; i++) {
            statefulTasks[i] = new StatefulTask(tasks[i], tasksCallback);
        }
        Context context = new ContextImpl(statefulTasks);

        for (StatefulTask task : statefulTasks) {
            threadPool.execute(task);
        }

        return context;
    }
}
