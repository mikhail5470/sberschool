package org.example.homework12;

import org.example.homework12.execution.Context;
import org.example.homework12.execution.ExecutionManager;
import org.example.homework12.execution.impl.ExecutionManagerImpl;
import org.example.homework12.task.Task;
import org.example.homework12.threadpool.FixedThreadPool;

import java.util.Random;

public class Main {

    public static void testCachedTask() {
        Task<String> task1 = new Task<>(() -> "result of " + Thread.currentThread());
        Task<String> task2 = new Task<>(() -> {
            throw new Exception();
        });

        for (int i = 0; i < 3; i++) {
            new Thread(() -> System.out.println(task1.get())).start();
        }
        for (int i = 0; i < 3; i++) {
            new Thread(() -> System.out.println(task2.get())).start();
        }
    }

    public static void printContext(Context context) {
        System.out.println(String.format(
                "completed: %d; failed: %d; interrupted: %d; isFinished: %b",
                context.getCompletedTaskCount(),
                context.getFailedTaskCount(),
                context.getInterruptedTaskCount(),
                context.isFinished()));
    }

    public static void testExecutionManager() throws InterruptedException {
        ExecutionManager executionManager = new ExecutionManagerImpl(new FixedThreadPool(5));

        Runnable[] tasks = new Runnable[10];

        tasks[0] = () -> {
            System.out.println("[STARTED][exception] " + Thread.currentThread());
            throw new RuntimeException("this is test exception");
        };

        for (int i = 1; i < tasks.length; i++) {
            tasks[i] = () -> {
                try {
                    System.out.println("[STARTED] " + Thread.currentThread());
                    Thread.sleep(new Random().nextInt(2000) + 100);
                    System.out.println("[FINISHED] " + Thread.currentThread());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
        }

        Context context = executionManager.execute(() -> System.out.println("[CALLBACK] DONE!"), tasks);

        System.out.println("START");
        printContext(context);
        Thread.sleep(500);

        System.out.println("CURRENT");
        printContext(context);
        context.interrupt();
        System.out.println("AFTER INTERRUPT ALL");
        printContext(context);
        Thread.sleep(3000);

        printContext(context);
    }

    public static void main(String[] args) throws InterruptedException {
        //testCachedTask();
        testExecutionManager();
    }
}
