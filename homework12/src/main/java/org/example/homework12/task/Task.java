package org.example.homework12.task;

import java.util.concurrent.Callable;

public class Task<T> {

    public enum State {
        UNKNOWN,
        OK,
        EXCEPTION
    }

    private final Callable<? extends T> callable;
    private State state;

    private volatile T cache;
    private volatile Exception exception;

    public Task(Callable<? extends T> callable) {
        this.state = State.UNKNOWN;
        this.callable = callable;
    }

    public T get() throws TaskResultException {

        if (state == State.UNKNOWN) {
            synchronized (this) {
                if (state == State.UNKNOWN) {
                    try {
                        System.out.println("calculating... thread: " + Thread.currentThread() + " callable: " + callable);
                        cache = callable.call();
                        state = State.OK;
                    } catch (Exception e) {
                        exception = e;
                        state = State.EXCEPTION;
                    }
                }

            }
        }

        if (state == State.EXCEPTION) throw new TaskResultException(exception);
        return cache;
    }

}
