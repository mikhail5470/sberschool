package org.example.homework12.task;

public class TaskResultException extends RuntimeException {
    public TaskResultException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskResultException(Throwable cause) {
        super(cause);
    }
}
