package org.example.homework12.threadpool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {

    private class Executor implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    tasks.take().run();
                } catch (InterruptedException ignore) {
                }
            }
        }
    }

    private final Thread[] threads;
    private final BlockingQueue<Runnable> tasks;

    public FixedThreadPool(int size) {
        if (size < 1) throw new IllegalArgumentException("size must be positive");

        this.tasks = new LinkedBlockingQueue<>();
        this.threads = new Thread[size];
        for (int i = 0; i < size; i++) {
            threads[i] = new Thread(new Executor());
        }
    }

    @Override
    public void start() {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    @Override
    public void execute(Runnable runnable) {
        try {
            tasks.put(runnable);
        } catch (InterruptedException ignore) {
        }
    }

}

