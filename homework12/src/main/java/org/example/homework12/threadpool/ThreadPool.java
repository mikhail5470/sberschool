package org.example.homework12.threadpool;

public interface ThreadPool {

    void start();

    void execute(Runnable runnable);

}
