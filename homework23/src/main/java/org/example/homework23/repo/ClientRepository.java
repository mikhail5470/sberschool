package org.example.homework23.repo;

import org.example.homework23.data.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

    Client getByPassportName(String name);

    List<Client> findByPassportNameLike(String name);

}
