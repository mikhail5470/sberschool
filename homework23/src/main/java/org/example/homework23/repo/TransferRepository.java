package org.example.homework23.repo;

import org.example.homework23.data.Transfer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {

    List<Transfer> findByTargetPassportNameLike(String targetName);

}
