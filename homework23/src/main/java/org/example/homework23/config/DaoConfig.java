package org.example.homework23.config;

import org.example.homework23.dao.data.ClientDao;
import org.example.homework23.dao.data.PassportDao;
import org.example.homework23.dao.data.TransferDao;
import org.example.homework23.dao.jpa.JpaClientDao;
import org.example.homework23.dao.jpa.JpaPassportDao;
import org.example.homework23.dao.jpa.JpaTransferDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class DaoConfig {

    @Bean
    public ClientDao clientDao(EntityManager entityManager) {
        return new JpaClientDao(entityManager);
    }

    @Bean
    public PassportDao passportDao(EntityManager entityManager) {
        return new JpaPassportDao(entityManager);
    }

    @Bean
    public TransferDao transferDao(EntityManager entityManager) {
        return new JpaTransferDao(entityManager);
    }

}
