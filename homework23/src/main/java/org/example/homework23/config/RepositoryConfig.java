package org.example.homework23.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("org.example.homework23.repo")
public class RepositoryConfig {
}
