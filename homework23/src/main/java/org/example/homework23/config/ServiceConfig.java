package org.example.homework23.config;

import org.example.homework23.dao.data.ClientDao;
import org.example.homework23.dao.data.PassportDao;
import org.example.homework23.dao.data.TransferDao;
import org.example.homework23.service.ClientService;
import org.example.homework23.service.TransferService;
import org.example.homework23.service.dao.DaoClientService;
import org.example.homework23.service.dao.DaoTransferService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(RepositoryConfig.class)
public class ServiceConfig {

    //Repository variant
    /*
    @Bean
    public ClientService clientService(ClientRepository clientRepository, PassportRepository passportRepository) {
        return new RepoClientService(clientRepository, passportRepository);
    }

    @Bean
    public TransferService transferService(TransferRepository transferRepository, ClientRepository clientRepository) {
        return new RepoTransferService(transferRepository, clientRepository);
    }
    */

    //DAO variant
    @Bean
    public ClientService clientService(ClientDao clientDao, PassportDao passportDao) {
        return new DaoClientService(clientDao, passportDao);
    }

    @Bean
    public TransferService transferService(TransferDao transferDao, ClientDao clientDao) {
        return new DaoTransferService(transferDao, clientDao);
    }

}
