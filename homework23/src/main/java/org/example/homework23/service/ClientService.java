package org.example.homework23.service;

import org.example.homework23.data.Client;
import org.example.homework23.data.Transfer;

import java.util.List;

public interface ClientService {

    List<Client> find(String name);

    List<Transfer> getTransfers(String name);

    void create(String name);

    void rename(String name, String newName);

    void remove(String name);

}
