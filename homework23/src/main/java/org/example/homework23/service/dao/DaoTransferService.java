package org.example.homework23.service.dao;

import org.example.homework23.dao.data.ClientDao;
import org.example.homework23.dao.data.TransferDao;
import org.example.homework23.data.Client;
import org.example.homework23.data.Transfer;
import org.example.homework23.service.TransferService;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class DaoTransferService implements TransferService {

    private final TransferDao transferDao;
    private final ClientDao clientDao;

    public DaoTransferService(TransferDao transferDao, ClientDao clientDao) {
        this.transferDao = transferDao;
        this.clientDao = clientDao;
    }

    @Override
    public List<Transfer> find(String targetName) {

        return (targetName == null) ? transferDao.getAll() : transferDao.findByTargetName(targetName);
    }

    @Override
    public void create(String name, String targetName, Double cash) {

        Client client = clientDao.getByName(name);
        if (client == null) throw new IllegalArgumentException("client not found");

        Client target = clientDao.getByName(targetName);
        if (target == null) throw new IllegalArgumentException("client not found");

        Transfer transfer = new Transfer();
        transfer.setTarget(target);
        transfer.setCash(cash);
        transfer.setDate(new Date());
        Hibernate.initialize(client.getTransfers());
        client.getTransfers().add(transfer);

        transferDao.create(transfer);
        clientDao.update(client);
    }

    @Override
    public void remove(Long id) {

        Transfer transfer = transferDao.get(id);
        if (transfer == null) throw new IllegalArgumentException("transfer not found");

        transferDao.delete(transfer);
    }
}
