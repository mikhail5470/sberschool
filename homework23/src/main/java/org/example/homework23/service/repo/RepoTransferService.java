package org.example.homework23.service.repo;

import org.example.homework23.data.Client;
import org.example.homework23.data.Transfer;
import org.example.homework23.repo.ClientRepository;
import org.example.homework23.repo.TransferRepository;
import org.example.homework23.service.TransferService;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class RepoTransferService implements TransferService {

    private final TransferRepository transferRepository;
    private final ClientRepository clientRepository;

    public RepoTransferService(TransferRepository transferRepository, ClientRepository clientRepository) {
        this.transferRepository = transferRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public List<Transfer> find(String targetName) {

        targetName = (targetName == null) ? "%" : "%" + targetName + "%";

        return transferRepository.findByTargetPassportNameLike(targetName);
    }

    @Override
    public void create(String name, String targetName, Double cash) {

        Client client = clientRepository.getByPassportName(name);
        if (client == null) throw new IllegalArgumentException("client not found");

        Client target = clientRepository.getByPassportName(targetName);
        if (target == null) throw new IllegalArgumentException("client not found");

        Transfer transfer = new Transfer();
        transfer.setTarget(target);
        transfer.setCash(cash);
        transfer.setDate(new Date());
        Hibernate.initialize(client.getTransfers());
        client.getTransfers().add(transfer);

        transferRepository.save(transfer);
    }

    @Override
    public void remove(Long id) {

        Transfer transfer = transferRepository.findById(id).get();
        if (transfer == null) throw new IllegalArgumentException("transfer not found");

        transferRepository.delete(transfer);
    }
}
