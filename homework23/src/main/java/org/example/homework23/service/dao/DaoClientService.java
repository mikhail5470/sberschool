package org.example.homework23.service.dao;


import org.example.homework23.dao.data.ClientDao;
import org.example.homework23.dao.data.PassportDao;
import org.example.homework23.data.Client;
import org.example.homework23.data.Passport;
import org.example.homework23.data.Transfer;
import org.example.homework23.service.ClientService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DaoClientService implements ClientService {

    private final ClientDao clientDao;
    private final PassportDao passportDao;

    @Autowired
    public DaoClientService(ClientDao clientDao, PassportDao passportDao) {

        this.clientDao = clientDao;
        this.passportDao = passportDao;
    }

    public List<Client> find(String name) {

        return (name == null) ? clientDao.getAll() : clientDao.findByName(name);
    }

    public List<Transfer> getTransfers(String name) {

        Client client = clientDao.getByName(name);
        if (client == null) return new ArrayList<>();

        Hibernate.initialize(client.getTransfers());

        return client.getTransfers();
    }

    public void create(String name) {

        Passport passport = new Passport();
        passport.setName(name);

        Client client = new Client();
        client.setPassport(passport);

        clientDao.create(client);
    }

    public void rename(String name, String newName) {

        Client client = clientDao.getByName(name);
        if (client == null) throw new IllegalArgumentException("client not found");

        client.getPassport().setName(newName);
        passportDao.update(client.getPassport());
    }

    public void remove(String name) {

        Client client = clientDao.getByName(name);
        if (client == null) throw new IllegalArgumentException("client not found");

        clientDao.delete(client);
    }
}
