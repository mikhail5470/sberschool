package org.example.homework23.service.repo;

import org.example.homework23.data.Client;
import org.example.homework23.data.Passport;
import org.example.homework23.data.Transfer;
import org.example.homework23.repo.ClientRepository;
import org.example.homework23.repo.PassportRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RepoClientService implements org.example.homework23.service.ClientService {

    private final ClientRepository clientRepository;
    private final PassportRepository passportRepository;

    @Autowired
    public RepoClientService(ClientRepository clientRepository, PassportRepository passportRepository) {

        this.clientRepository = clientRepository;
        this.passportRepository = passportRepository;
    }

    public List<Client> find(String name) {

        return (name == null) ? (List<Client>) clientRepository.findAll() : clientRepository.findByPassportNameLike("%" + name + "%");
    }

    public List<Transfer> getTransfers(String name) {

        Client client = clientRepository.getByPassportName(name);
        if (client == null) return new ArrayList<>();

        Hibernate.initialize(client.getTransfers());

        return client.getTransfers();
    }

    public void create(String name) {

        Passport passport = new Passport();
        passport.setName(name);
        passportRepository.save(passport);

        Client client = new Client();
        client.setPassport(passport);
        clientRepository.save(client);
    }

    public void rename(String name, String newName) {

        Client client = clientRepository.getByPassportName(name);
        if (client == null) throw new IllegalArgumentException("client not found");

        client.getPassport().setName(newName);
    }

    public void remove(String name) {

        Client client = clientRepository.getByPassportName(name);
        if (client == null) throw new IllegalArgumentException("client not found");

        clientRepository.delete(client);
    }

}
