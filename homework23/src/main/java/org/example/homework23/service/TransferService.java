package org.example.homework23.service;

import org.example.homework23.data.Transfer;

import java.util.List;

public interface TransferService {

    List<Transfer> find(String targetName);

    void create(String name, String targetName, Double cash);

    void remove(Long id);

}
