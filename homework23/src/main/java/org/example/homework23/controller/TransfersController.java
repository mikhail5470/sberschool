package org.example.homework23.controller;

import org.example.homework23.data.Transfer;
import org.example.homework23.service.TransferService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transfers")
public class TransfersController {

    private final TransferService transferService;

    public TransfersController(TransferService transferService) {
        this.transferService = transferService;
    }

    @RequestMapping("/list")
    public List<Transfer> list(
            @RequestParam(name = "target_name", required = false) String targetName
    ) {

        return transferService.find(targetName);
    }

    @RequestMapping("/create")
    public void create(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "target_name") String targetName,
            @RequestParam(name = "cash") Double cash
    ) {

        transferService.create(name, targetName, cash);
    }

    @RequestMapping("/remove")
    public void remove(
            @RequestParam(name = "id") Long id
    ) {

        transferService.remove(id);
    }

}
