package org.example.homework23.controller;

import org.example.homework23.data.Client;
import org.example.homework23.data.Transfer;
import org.example.homework23.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/clients")
public class ClientsController {

    private final ClientService clientService;

    @Autowired
    public ClientsController(ClientService clientService) {
        this.clientService = clientService;
    }

    @RequestMapping("/list")
    public List<Client> list(
            @RequestParam(name = "name", required = false) String name
    ) {

        return clientService.find(name);
    }

    @RequestMapping("/transfers")
    public List<Transfer> transfers(
            @RequestParam(name = "name") String name
    ) {

        return clientService.getTransfers(name);
    }

    @RequestMapping("/create")
    public void create(
            @RequestParam(name = "name") String name
    ) {

        clientService.create(name);
    }

    @RequestMapping("/rename")
    public void rename(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "new_name") String newName
    ) {

        clientService.rename(name, newName);
    }

    @RequestMapping("/remove")
    public void remove(
            @RequestParam(name = "name") String name
    ) {

        clientService.remove(name);
    }

}
