package org.example.homework23.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return
                "/clients/list?name=[name] - find client<br />" +
                        "/clients/transfers?name=[name] - get client transfers<br />" +
                        "/clients/rename?name=[name]&new_name=[new_name] - rename client (passport)<br />" +
                        "/clients/remove?name=[name] - remove client<br />" +
                        "/transfers/list?target_name=[target_name] - get transfers by target name<br />" +
                        "/transfers/create?name=[name]&target_name=[target_name]&cash=[cash] - create transfer<br />" +
                        "/transfers/remove?id=[id] - remove transfer by id";

    }

}
