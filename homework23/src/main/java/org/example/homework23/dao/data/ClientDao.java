package org.example.homework23.dao.data;

import org.example.homework23.dao.Dao;
import org.example.homework23.data.Client;

import java.util.List;

public interface ClientDao extends Dao<Client, Long> {

    Client getByName(String name);

    List<Client> findByName(String name);

}
