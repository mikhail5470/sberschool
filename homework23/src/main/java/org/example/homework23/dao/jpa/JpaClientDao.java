package org.example.homework23.dao.jpa;

import org.example.homework23.dao.data.ClientDao;
import org.example.homework23.data.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Transactional
@Repository
public class JpaClientDao extends AbstractJpaDao<Client, Long> implements ClientDao {

    @Autowired
    public JpaClientDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Client getByName(String name) {

        List<Client> clients = getEntityManager().createQuery("SELECT c FROM Client c WHERE c.passport.name = :name", Client.class).setParameter("name", name).getResultList();
        if (clients.isEmpty()) return null;

        return clients.get(0);
    }

    @Override
    public List<Client> findByName(String name) {

        return getEntityManager().createQuery("SELECT c FROM Client c WHERE c.passport.name LIKE :name", Client.class).setParameter("name", "%" + name + "%").getResultList();
    }
}
