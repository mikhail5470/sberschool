package org.example.homework23.dao.jpa;

import org.example.homework23.dao.Dao;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractJpaDao<T, K extends Serializable> implements Dao<T, K> {

    private final Class<T> entityClass;
    private final EntityManager entityManager;

    protected AbstractJpaDao(EntityManager entityManager) {
        this.entityClass =
                (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.entityManager = entityManager;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public T get(K key) {
        return entityManager.find(entityClass, key);
    }

    @Override
    public Long getCount() {
        return entityManager.createQuery("SELECT COUNT(*) FROM " + entityClass.getSimpleName(), Long.class).getSingleResult();
    }

    @Override
    public List<T> getAll() {
        return entityManager.createQuery("SELECT t FROM " + entityClass.getSimpleName() + " t", entityClass).getResultList();
    }

    @Override
    public void update(T dataObject) {
        entityManager.merge(dataObject);
    }

    @Override
    public void create(T dataObject) {
        entityManager.persist(dataObject);
    }

    @Override
    public void delete(T dataObject) {
        entityManager.remove(dataObject);
    }

    @Override
    public void deleteAll() {
        entityManager.createQuery("DELETE FROM " + entityClass.getSimpleName()).executeUpdate();
    }

}
