package org.example.homework23.dao.data;

import org.example.homework23.dao.Dao;
import org.example.homework23.data.Transfer;

import java.util.List;

public interface TransferDao extends Dao<Transfer, Long> {

    List<Transfer> findByTargetName(String targetName);

}
