package org.example.homework23.dao;

import java.io.Serializable;
import java.util.List;

public interface Dao<T, K extends Serializable> {

    T get(K key);

    Long getCount();

    List<T> getAll();

    void update(T dataObject);

    void create(T dataObject);

    void delete(T dataObject);

    void deleteAll();

}
