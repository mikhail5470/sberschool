package org.example.homework23.dao.jpa;

import org.example.homework23.dao.data.PassportDao;
import org.example.homework23.data.Passport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Transactional
@Repository
public class JpaPassportDao extends AbstractJpaDao<Passport, Long> implements PassportDao {

    @Autowired
    public JpaPassportDao(EntityManager entityManager) {
        super(entityManager);
    }
}
