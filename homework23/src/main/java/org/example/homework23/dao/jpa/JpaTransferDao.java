package org.example.homework23.dao.jpa;

import org.example.homework23.dao.data.TransferDao;
import org.example.homework23.data.Transfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Transactional
@Repository
public class JpaTransferDao extends AbstractJpaDao<Transfer, Long> implements TransferDao {

    @Autowired
    public JpaTransferDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Transfer> findByTargetName(String targetName) {
        return getEntityManager().createQuery("SELECT t FROM Transfer t WHERE t.target.passport.name LIKE :name", Transfer.class).setParameter("name", "%" + targetName + "%").getResultList();
    }
}
