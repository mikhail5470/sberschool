package org.example.homework23.dao.data;

import org.example.homework23.dao.Dao;
import org.example.homework23.data.Passport;

public interface PassportDao extends Dao<Passport, Long> {
}
