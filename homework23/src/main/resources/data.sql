INSERT INTO passports (id, name) VALUES
    (1, 'Иванов Борис Борисович'),
    (2, 'Петров Иван Иванович');

INSERT INTO clients (id, passport_id) VALUES
    (1, 1), (2, 2);

INSERT INTO transfers (id, client_id, target_client_id, cash, date) VALUES
    (1, 1, 2, 100.0, '2000-09-12 12:00:00.000'),
    (2, 1, 2, 200.0, '2002-05-15 08:00:00.000'),
    (3, 2, 1, 100000.0, '2012-02-28 23:15:00.000');

