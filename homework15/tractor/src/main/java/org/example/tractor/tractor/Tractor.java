package org.example.tractor.tractor;

import org.example.tractor.geometry.Field;
import org.example.tractor.geometry.Movable;
import org.example.tractor.geometry.Item;
import org.example.tractor.geometry.Orientation;
import org.example.tractor.geometry.Point;

public class Tractor extends Item implements Movable {

    private final Field field;

    public Tractor(Field field, Point position, Orientation orientation) {
        super(position, orientation);
        this.field = field;
    }

    public Tractor(Field field) {
        super();
        this.field = field;
    }

    public void move() {
        Point position = getPosition().add(getOrientation().getMatrix());
        if (!field.isInside(position)) throw new TractorInDitchException();

        setPosition(position);
    }

    public void turn() {
        setOrientation(getOrientation().turn(1));
    }

}
