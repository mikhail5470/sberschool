package org.example.tractor.geometry;

public interface Movable {

    void move();
    void turn();

}
