package org.example.tractor.geometry;

public class Field {

    private final Point size;

    public Field(Point size) {
        if (size.getX() < 1 || size.getY() < 1) throw new IllegalArgumentException();

        this.size = size;
    }

    public Point getSize() {
        return size;
    }

    public boolean isInside(Point point) {
        return
            point.getX() >= 0 && point.getX() < size.getX() &&
            point.getY() >= 0 && point.getY() < size.getY();
    }

}
