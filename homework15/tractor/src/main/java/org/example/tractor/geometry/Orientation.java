package org.example.tractor.geometry;

import java.util.HashMap;

public enum Orientation {
    NORTH(0, 1), WEST(-1, 0), SOUTH(0, -1), EAST(1, 0);

    private final Point matrix;

    Orientation(int x, int y) {
        this.matrix = new Point(x, y);
    }

    public Point getMatrix() {
        return matrix;
    }

    public Orientation turn(int amount) {
        Orientation[] values = Orientation.values();
        return values[(this.ordinal() + amount) % values.length];
    }



}
