package org.example.tractor.geometry;

public abstract class Item {

    private static final Point DEFAULT_POSITION = new Point(0, 0);
    private static final Orientation DEFAULT_ORIENTATION = Orientation.NORTH;

    private Point position;
    private Orientation orientation;

    public Item(Point position, Orientation orientation) {
        this.position = position;
        this.orientation = orientation;
    }

    public Item(Point position) {
        this(position, DEFAULT_ORIENTATION);
    }

    public Item() {
        this(DEFAULT_POSITION, DEFAULT_ORIENTATION);
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }
}
