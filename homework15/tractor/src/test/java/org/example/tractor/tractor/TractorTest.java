package org.example.tractor.tractor;

import org.example.tractor.geometry.Field;
import org.example.tractor.geometry.Orientation;
import org.example.tractor.geometry.Point;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TractorTest {

    private Field field;
    private Tractor tractor;

    @Before
    public void setUp() {
        field = new Field(new Point(5, 7));
        tractor = new Tractor(field);
    }

    @Test
    public void moveLimit() {
        tractor.setPosition(new Point(0, 0));
        tractor.setOrientation(Orientation.EAST);
        for (int i = 0; i < field.getSize().getX()-1; i++) {
            tractor.move();
        }
        tractor.turn();
        for (int i = 0; i < field.getSize().getY()-1; i++) {
            tractor.move();
        }
        Assert.assertEquals(tractor.getPosition(), field.getSize().add(new Point(-1, -1)));
    }

    @Test(expected = TractorInDitchException.class)
    public void moveInDitchException() {
        tractor.setPosition(new Point(0, 0));
        tractor.setOrientation(Orientation.EAST);
        for (int i = 0; i < field.getSize().getX(); i++) {
            tractor.move();
        }
    }

    @Test
    public void turnCircle() {
        Orientation current = tractor.getOrientation();
        for (int i = 0; i < 4; i++) {
            tractor.turn();
        }
        Assert.assertEquals(current, tractor.getOrientation());
    }
}