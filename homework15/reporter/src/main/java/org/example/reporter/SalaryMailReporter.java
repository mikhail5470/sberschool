package org.example.reporter;

import javafx.util.Pair;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class SalaryMailReporter {

    private static final String HOST = "mail.google.com";

    private final Connection connection;
    private final JavaMailSenderImpl sender;

    public SalaryMailReporter(Connection databaseConnection) {
        this.connection = databaseConnection;
        sender = new JavaMailSenderImpl();
        sender.setHost(HOST);
    }

    private ArrayList<Pair<String, Double>> getData(String departmentID, LocalDate dateFrom, LocalDate dateTo) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(
                "SELECT emp.id as id, emp.name AS name, SUM(sp.salary) AS salary " +
                        "FROM employees AS emp LEFT JOIN salary_payments AS sp ON emp.id = sp.employee_id " +
                        "WHERE emp.department_id = ? AND sp.date >= ? AND sp.date <= ? " +
                        "GROUP BY id, name"
        );
        ps.setString(0, departmentID);
        ps.setDate(1, Date.valueOf(dateFrom));
        ps.setDate(2, Date.valueOf(dateTo));
        ResultSet result = ps.executeQuery();

        ArrayList<Pair<String, Double>> data = new ArrayList<>();
        while (result.next()) {
            data.add(new Pair<>(result.getString("name"), result.getDouble("salary")));
        }
        return data;
    }

    private String buildHtml(ArrayList<Pair<String, Double>> data) {
        StringBuilder html = new StringBuilder();
        html.append("<html><body><table><tr><td>Employee</td><td>Salary</td></tr>");

        double total = 0;
        for (Pair<String, Double> entry : data) {
            html.append("<tr>");
            html.append("<td>").append(entry.getKey()).append("</td>");
            html.append("<td>").append(entry.getValue()).append("</td>");
            html.append("</tr>");
            total += entry.getValue();
        }
        html.append("<tr><td>Total</td><td>").append(total).append("</td></tr>");
        html.append("</table></body></html>");

        return html.toString();
    }

    private void send(String html, String recipients) throws MessagingException {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(recipients);
        helper.setText(html, true);
        helper.setSubject("Monthly department salary report");
        sender.send(message);
    }

    public void report(String departmentId, LocalDate dateFrom, LocalDate dateTo, String recipients) {
        try {
            send(buildHtml(getData(departmentId, dateFrom, dateTo)), recipients);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
