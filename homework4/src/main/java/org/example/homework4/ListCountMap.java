package org.example.homework4;

import java.util.*;

public class ListCountMap<T> implements CountMap<T> {

    static class Entry<T> implements CountMap.Entry<T> {
        final T item;
        int count;

        Entry(T item) {
            this.item = item;
            this.count = 0;
        }

        @Override
        public T getItem() {
            return item;
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public int setCount(int count) {
            int oldCount = this.count;
            this.count = count;
            return oldCount;
        }

        @Override
        public String toString() {
            return item + ":" + count;
        }
    }

    private ArrayList<Entry<T>> entries;

    public ListCountMap() {
        entries = new ArrayList<>();
    }

    private Entry<T> getEntry(T o) {
        for (Entry<T> entry : entries) {
            if (entry.getItem() == o) return entry;
        }
        return null;
    }

    @Override
    public void add(T o) {
        Entry<T> entry = getEntry(o);
        if (entry == null) {
            entry = new Entry<>(o);
            entries.add(entry);
        }

        entry.setCount(entry.getCount() + 1);
    }

    @Override
    public void add(T o, int count) {
        Entry<T> entry = getEntry(o);
        if (entry == null) {
            entry = new Entry<>(o);
            entries.add(entry);
        }

        entry.setCount(entry.getCount() + count);
    }

    @Override
    public int getCount(T o) {
        Entry<T> entry = getEntry(o);
        if (entry == null) return 0;

        return entry.getCount();
    }

    @Override
    public int remove(T o) {
        Entry<T> entry = getEntry(o);
        if (entry == null) return 0;

        int oldCount = entry.getCount();
        if (oldCount == 1) {
            entries.remove(entry);
        } else {
            entry.setCount(oldCount - 1);
        }

        return oldCount;
    }

    @Override
    public int size() {
        return entries.size();
    }

    @Override
    public List<CountMap.Entry<T>> entryList() {
        return new ArrayList<>(entries);
    }

    @Override
    public void addAll(CountMap<T> source) {
        for (CountMap.Entry<T> entry : source.entryList()) {
            add(entry.getItem(), entry.getCount());
        }
    }

    @Override
    public Map<T, Integer> toMap() {
        HashMap<T, Integer> map = new HashMap<>();
        for (Entry<T> entry : entries) {
            map.put(entry.getItem(), entry.getCount());
        }

        return map;
    }

    @Override
    public void toMap(Map<T, Integer> destination) {
        for (Entry<T> entry : entries) {
            destination.put(entry.getItem(), entry.getCount());
        }
    }

    @Override
    public String toString() {
        Iterator<CountMap.Entry<T>> i = entryList().iterator();
        if (!i.hasNext())
            return "{}";

        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (; ; ) {
            sb.append(i.next().toString());
            if (!i.hasNext())
                return sb.append('}').toString();
            sb.append(',').append(' ');
        }
    }

}
