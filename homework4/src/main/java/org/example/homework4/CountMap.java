package org.example.homework4;

import java.util.List;
import java.util.Map;

public interface CountMap<T> {

    interface Entry<T> {
        T getItem();

        int getCount();

        int setCount(int count);
    }

    void add(T e);

    void add(T e, int count);

    int getCount(T e);

    int remove(T e);

    int size();

    List<Entry<T>> entryList();

    void addAll(CountMap<T> source);

    Map<T, Integer> toMap();

    void toMap(Map<T, Integer> destination);

}
