package org.example.homework4;

public class Main {
    public static void main(String[] args) {

        ListCountMap<String> lcm = new ListCountMap<>();
        lcm.add("item1");
        lcm.add("item2");
        lcm.add("item2");
        lcm.add("item2");
        lcm.add("item3");
        lcm.add("item4");
        lcm.add("item4");
        lcm.add("item5", 4);
        System.out.println("source: \n" + lcm);

        lcm.remove("item4");
        lcm.remove("item1");
        System.out.println("removed item4, removed item1: \n" + lcm);

        System.out.println("item3 count:\n" + lcm.getCount("item3"));

        System.out.println("size:\n" + lcm.size());

        ListCountMap<String> lcm2 = new ListCountMap<>();
        lcm2.add("item2");
        lcm2.add("item3", 7);
        lcm.addAll(lcm2);

        System.out.println("addAll " + lcm2 + ":\n" + lcm);

        System.out.println("toMap: \n" + lcm.toMap());

    }
}
