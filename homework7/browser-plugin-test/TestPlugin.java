import org.example.homework7browser.plugin.Plugin;

public class TestPlugin implements Plugin {

    @Override
    public void run() {
        System.out.println("TestPlugin is running");
        Example example = new Example();
        example.run();
    }
}
