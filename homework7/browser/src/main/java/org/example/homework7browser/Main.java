package org.example.homework7browser;

import org.example.homework7browser.plugin.Plugin;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class Main {

    private static final String PLUGINS_PATH = "homework7\\browser\\plugins\\";

    private static final String CIPHER_TRANSFORMATION = "AES";
    private static final String CIPHER_KEY_ALGORITHM = "AES";
    private static final Key CIPHER_KEY = new SecretKeySpec("0123456789ABCDEF".getBytes(), CIPHER_KEY_ALGORITHM);
    private static final String ENCRYPTED_CLASS_PATH = "homework7\\browser\\encrypted-classes\\";
    private static final String ENCRYPTED_CLASS_NAME = "SomeForEncrypt";

    private static void testPlugins() throws ClassNotFoundException, MalformedURLException, InstantiationException, IllegalAccessException {
        PluginManager pluginManager = new PluginManager(PLUGINS_PATH);
        Plugin testPlugin = pluginManager.load("test", "TestPlugin");
        testPlugin.run();

        Plugin test2Plugin = pluginManager.load("test2", "Test2Plugin");
        test2Plugin.run();
    }

    private static void createTestEncryptedClass(Cipher cipher) throws IOException, BadPaddingException, IllegalBlockSizeException {

        byte[] decryptedBytes = Files.readAllBytes(Paths.get(ENCRYPTED_CLASS_PATH + "\\" + ENCRYPTED_CLASS_NAME + ".class"));
        byte[] encryptedBytes = cipher.doFinal(decryptedBytes);
        Files.write(Paths.get(ENCRYPTED_CLASS_PATH + "\\" + ENCRYPTED_CLASS_NAME + ".classe"), encryptedBytes);

    }

    private static void testEncryptedClassLoader() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, CIPHER_KEY);
        try {
            createTestEncryptedClass(cipher);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("class " + ENCRYPTED_CLASS_NAME + " encrypted");

        cipher.init(Cipher.DECRYPT_MODE, CIPHER_KEY);
        EncryptedClassLoader ecl = new EncryptedClassLoader(ENCRYPTED_CLASS_PATH, cipher, Main.class.getClassLoader());
        Class clazz = ecl.findClass(ENCRYPTED_CLASS_NAME);
        System.out.println("class " + ENCRYPTED_CLASS_NAME + " decrypted");
        Object o = clazz.newInstance();

        Method m = clazz.getMethod("run");
        m.invoke(o);
    }

    public static void main(String[] args) {

        try {
            testPlugins();
            testEncryptedClassLoader();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
