package org.example.homework7browser;

import org.example.homework7browser.plugin.Plugin;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;

public class PluginManager {

    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) throws MalformedURLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        URL[] urls = new URL[]{
                Paths.get(pluginRootDirectory + pluginName).toUri().toURL()
        };
        URLClassLoader urlClassLoader = new URLClassLoader(urls);
        Class<?> pluginClass = urlClassLoader.loadClass(pluginClassName);

        return (Plugin) pluginClass.newInstance();
    }
}
