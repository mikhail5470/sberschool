package org.example.homework7browser;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class EncryptedClassLoader extends ClassLoader {

    private final String path;
    private final Cipher cipher;

    public EncryptedClassLoader(String path, Cipher cipher, ClassLoader parent) {
        super(parent);
        this.path = path;
        this.cipher = cipher;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        Class<?> clazz;
        try {
            byte[] encryptedBytes = Files.readAllBytes(Paths.get(path + "\\" + name + ".classe"));
            byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
            clazz = defineClass(name, decryptedBytes, 0, decryptedBytes.length);

        } catch (Exception e) {
            throw new ClassNotFoundException(e.getMessage());
        }

        return clazz;
    }
}
