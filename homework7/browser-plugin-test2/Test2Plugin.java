import org.example.homework7browser.plugin.Plugin;

public class Test2Plugin implements Plugin {

    @Override
    public void run() {
        System.out.println("Test2Plugin is running");
        Example example = new Example();
        example.run();
    }
}
