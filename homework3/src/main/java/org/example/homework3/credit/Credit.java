package org.example.homework3.credit;

import org.example.homework3.client.Client;

public class Credit {

    private Client client;
    private double amount;
    private double repaid;

    public Credit(Client client, double amount) {
        this.client = client;
        this.amount = amount;
        this.repaid = 0d;
    }

    public Client getClient() {
        return client;
    }

    public double getAmount() {
        return amount;
    }

    public double getRepaid() {
        return repaid;
    }

    public void repay(double amount) {
        this.repaid += amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Credit credit = (Credit) o;

        if (Double.compare(credit.amount, amount) != 0) return false;
        if (Double.compare(credit.repaid, repaid) != 0) return false;
        return client.equals(credit.client);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = client.hashCode();
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(repaid);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "credit " + amount;
    }

}
