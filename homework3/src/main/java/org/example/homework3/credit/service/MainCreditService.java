package org.example.homework3.credit.service;

import org.example.homework3.client.Client;
import org.example.homework3.credit.Credit;

import java.util.*;

public class MainCreditService extends AbstractCreditService {

    private List<Credit> credits;

    public MainCreditService(List<Credit> credits) {
        this.credits = credits;
    }

    public List<Credit> getCredits() {
        return credits;
    }

    public List<Credit> getCredits(Client client) {
        List<Credit> clientCredits = new ArrayList<>();
        for (Credit credit : credits) {
            if (credit.getClient() == client) clientCredits.add(credit);
        }

        return clientCredits;
    }

    public double getCreditsAmount(Client client) {
        double amount = 0d;
        for (Credit credit : getCredits(client)) {
            amount += credit.getAmount();
        }

        return amount;
    }

    public Map<Client, List<Credit>> getCreditsByClient() {
        Map<Client, List<Credit>> clientsCredits = new HashMap<>();
        for (Credit credit : credits) {
            Client client = credit.getClient();
            if (!clientsCredits.containsKey(client)) clientsCredits.put(client, new ArrayList<>());
            clientsCredits.get(client).add(credit);
        }

        return clientsCredits;
    }

    public Map<Client, List<Credit>> getCreditsByClientSortedByClient() {
        List<Map.Entry<Client, List<Credit>>> list = new ArrayList<>(getCreditsByClient().entrySet());
        list.sort((o1, o2) -> o1.getKey().compareTo(o2.getKey()));

        Map<Client, List<Credit>> result = new LinkedHashMap<>();
        for (Map.Entry<Client, List<Credit>> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public Map<Client, List<Credit>> getCreditsByClientSortedByAmount() {
        List<Map.Entry<Client, List<Credit>>> list = new ArrayList<>(getCreditsByClient().entrySet());
        list.sort((o1, o2) -> -(Double.compare(getCreditsAmount(o1.getKey()), getCreditsAmount(o2.getKey()))));

        Map<Client, List<Credit>> result = new LinkedHashMap<>();
        for (Map.Entry<Client, List<Credit>> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public Map<Client, List<Credit>> getCreditsByClientSortedByClientAndAmount() {
        List<Map.Entry<Client, List<Credit>>> list = new ArrayList<>(getCreditsByClient().entrySet());
        Comparator<Map.Entry<Client, List<Credit>>> cmp = (o1, o2) -> {
            int result = o1.getKey().compareTo(o2.getKey());
            if (result == 0)
                result = -(Double.compare(getCreditsAmount(o1.getKey()), getCreditsAmount(o2.getKey())));
            return result;
        };
        list.sort(cmp);

        Map<Client, List<Credit>> result = new LinkedHashMap<>();
        for (Map.Entry<Client, List<Credit>> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }


}
