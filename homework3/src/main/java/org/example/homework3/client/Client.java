package org.example.homework3.client;

public class Client implements Comparable<Client> {

    private Passport passport;

    public Client(Passport passport) {
        this.passport = passport;
    }

    public Passport getPassport() {
        return passport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        return passport.equals(client.passport);
    }

    @Override
    public int hashCode() {
        return passport.hashCode();
    }

    @Override
    public String toString() {
        return passport.toString();
    }

    @Override
    public int compareTo(Client c) {
        return getPassport().getName().toString().compareTo(c.getPassport().getName().toString());
    }
}
