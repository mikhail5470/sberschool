package org.example.homework3.client;

public class PersonName {

    private String firstName;
    private String secondName;
    private String thirdName;

    public PersonName(String firstName, String secondName, String thirdName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.thirdName = thirdName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getThirdName() {
        return thirdName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonName that = (PersonName) o;

        if (!firstName.equals(that.firstName)) return false;
        if (!secondName.equals(that.secondName)) return false;
        return thirdName.equals(that.thirdName);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + secondName.hashCode();
        result = 31 * result + thirdName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return firstName + " " + secondName + " " + thirdName;
    }
}
