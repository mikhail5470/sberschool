package org.example.homework3.client;

public class Passport {

    private String id;
    private PersonName name;

    public Passport(String id, PersonName name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public PersonName getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passport passport = (Passport) o;

        if (!id.equals(passport.id)) return false;
        return name.equals(passport.name);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    public String toString() {
        return id + " " + name.toString();
    }

}
