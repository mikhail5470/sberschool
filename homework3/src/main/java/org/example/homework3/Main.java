package org.example.homework3;

import org.example.homework3.client.Client;
import org.example.homework3.client.Passport;
import org.example.homework3.client.PersonName;
import org.example.homework3.credit.Credit;
import org.example.homework3.credit.service.MainCreditService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Client cl1 = new Client(new Passport("457345", new PersonName("Иван", "Петров", "Ярославович")));
        Client cl2 = new Client(new Passport("356547", new PersonName("Борис", "Иванов", "Михайлович")));
        Client cl3 = new Client(new Passport("453463", new PersonName("Aндрей", "Борисов", "Геннадьевич")));

        List<Credit> credits = new ArrayList<>();
        credits.add(new Credit(cl1, 5_000_000d));
        credits.add(new Credit(cl1, 200_000d));
        credits.add(new Credit(cl2, 2_500_000d));
        credits.add(new Credit(cl2, 50_000d));
        credits.add(new Credit(cl2, 20_000d));
        credits.add(new Credit(cl3, 5_000d));

        MainCreditService creditService = new MainCreditService(credits);

        System.out.println("credits:");
        System.out.println(creditService.getCreditsByClient());

        System.out.println("credits_sorted_by_client:");
        System.out.println(creditService.getCreditsByClientSortedByClient());

        System.out.println("credits_sorted_by_amount:");
        System.out.println(creditService.getCreditsByClientSortedByAmount());

        System.out.println("credits_sorted_by_client_and_amount:");
        System.out.println(creditService.getCreditsByClientSortedByClientAndAmount());

    }



}
