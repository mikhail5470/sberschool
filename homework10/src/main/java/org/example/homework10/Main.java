package org.example.homework10;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> intList = Arrays.asList(5, 11, 25, 36, 43, 21, 29, 4, 4, 1, 2);

        System.out.println(Streams.of(intList).
                filter(e -> e > 9).
                transform(e -> e + 48).
                toMap(e -> e, e -> (char) e.byteValue())
        );

    }
}
