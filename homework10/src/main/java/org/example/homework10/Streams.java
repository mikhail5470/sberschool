package org.example.homework10;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Streams<T> {

    private Supplier<T> supplier;

    public Streams(Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public static <T> Streams<T> of(Collection<T> collection) {
        Iterator<T> iterator = collection.iterator();
        return new Streams<>(iterator::next);
    }

    public Streams<T> filter(Predicate<T> predicate) {
        return new Streams<>(() -> {
            T next;
            do {
                next = supplier.get();
            } while (!(predicate.test(next)));
            return next;
        });
    }

    public <R> Streams<R> transform(Function<? super T, ? extends R> transformer) {
        return new Streams<>(() -> transformer.apply(supplier.get()));
    }

    public <K, V> Map<K, V> toMap(Function<? super T, ? extends K> keyDefiner, Function<? super T, ? extends V> valueDefiner) {
        Map<K, V> map = new HashMap<>();
        while (true) {
            try {
                T element = supplier.get();
                K key = keyDefiner.apply(element);
                V value = valueDefiner.apply(element);
                map.put(key, value);
            } catch (NoSuchElementException e) {
                break;
            }
        }

        return map;
    }

}
